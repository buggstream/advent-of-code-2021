use advent_of_code_2021::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::cmp::Ordering;
use std::error::Error;
use std::ops::RangeInclusive;
use std::str::FromStr;

lazy_static! {
    static ref INSTRUCTIONS: Regex =
        Regex::new(r"(?m)^(on|off) x=(-?\d+)..(-?\d+),y=(-?\d+)..(-?\d+),z=(-?\d+)..(-?\d+)$")
            .unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(22)?;
    let reboot_steps = parse_reboot_steps(&input)?;
    let volume = puzzle_one(&reboot_steps);
    println!("{}", volume);
    let volume = puzzle_two(&reboot_steps);
    println!("{}", volume);

    Ok(())
}

fn puzzle_one(steps: &[(bool, Cuboid)]) -> i64 {
    let acceptable_steps: Vec<_> = steps
        .iter()
        .filter(|(_, cuboid)| {
            (*cuboid.range_x.start() >= -50 && *cuboid.range_x.end() <= 50)
                && (*cuboid.range_y.start() >= -50 && *cuboid.range_y.end() <= 50)
                && (*cuboid.range_z.start() >= -50 && *cuboid.range_z.end() <= 50)
        })
        .cloned()
        .collect();

    puzzle_two(&acceptable_steps)
}

fn puzzle_two(steps: &[(bool, Cuboid)]) -> i64 {
    let mut positive_spaces = Vec::new();
    let mut negative_spaces = Vec::new();

    for (turn_on, new_cuboid) in steps {
        let mut cuboids_to_add = Vec::new();

        for negative_cuboid in negative_spaces.iter() {
            if let Some(intersection) = new_cuboid.intersect(negative_cuboid) {
                cuboids_to_add.push(intersection);
            }
        }

        for positive_cuboid in positive_spaces.iter() {
            if let Some(intersection) = new_cuboid.intersect(positive_cuboid) {
                negative_spaces.push(intersection);
            }
        }

        positive_spaces.extend(cuboids_to_add);

        if *turn_on {
            positive_spaces.push(new_cuboid.clone());
        }
    }

    let positive_volume: i64 = positive_spaces.iter().map(|cuboid| cuboid.volume()).sum();
    let negative_volume: i64 = negative_spaces.iter().map(|cuboid| cuboid.volume()).sum();

    positive_volume - negative_volume
}

fn parse_reboot_steps(input: &str) -> Result<Vec<(bool, Cuboid)>, Box<dyn Error>> {
    let mut instructions = Vec::new();

    for captures in INSTRUCTIONS.captures_iter(input) {
        let on = &captures[1] == "on";
        let x_start = i64::from_str(&captures[2])?;
        let x_end = i64::from_str(&captures[3])?;
        let y_start = i64::from_str(&captures[4])?;
        let y_end = i64::from_str(&captures[5])?;
        let z_start = i64::from_str(&captures[6])?;
        let z_end = i64::from_str(&captures[7])?;

        let cuboid = Cuboid {
            range_x: x_start..=x_end,
            range_y: y_start..=y_end,
            range_z: z_start..=z_end,
        };

        instructions.push((on, cuboid));
    }

    Ok(instructions)
}

fn overlap(a: &RangeInclusive<i64>, b: &RangeInclusive<i64>) -> Option<RangeInclusive<i64>> {
    let comparisons = (
        a.end().cmp(b.start()),
        a.start().cmp(b.end()),
        a.start().cmp(b.start()),
        a.end().cmp(b.end()),
    );

    // |---A---|   |---B---|     (non-overlapping)
    // |---B---|   |---A---|     (non-overlapping)
    //  |---B-|##A##|-B---|      (A within B)
    //  |---A-|##B##|-A---|      (B within A)
    //   |--A--|###|--B--|       (Right of A overlap with left of B)
    //   |--B--|###|--A--|       (Right of B overlap with left of A)
    match comparisons {
        (Ordering::Less, _, _, _) => None,
        (_, Ordering::Greater, _, _) => None,
        (_, _, Ordering::Greater | Ordering::Equal, Ordering::Less | Ordering::Equal) => {
            Some(a.clone())
        }
        (_, _, Ordering::Less | Ordering::Equal, Ordering::Greater | Ordering::Equal) => {
            Some(b.clone())
        }
        (_, _, Ordering::Less, Ordering::Less) => Some(*b.start()..=*a.end()),
        (_, _, Ordering::Greater, Ordering::Greater) => Some(*a.start()..=*b.end()),
    }
}

#[derive(Debug, Clone)]
struct Cuboid {
    range_x: RangeInclusive<i64>,
    range_y: RangeInclusive<i64>,
    range_z: RangeInclusive<i64>,
}

impl Cuboid {
    fn volume(&self) -> i64 {
        (self.range_x.end() - self.range_x.start() + 1)
            * (self.range_y.end() - self.range_y.start() + 1)
            * (self.range_z.end() - self.range_z.start() + 1)
    }

    fn intersect(&self, other: &Cuboid) -> Option<Cuboid> {
        Some(Cuboid {
            range_x: overlap(&self.range_x, &other.range_x)?,
            range_y: overlap(&self.range_y, &other.range_y)?,
            range_z: overlap(&self.range_z, &other.range_z)?,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::{overlap, parse_reboot_steps, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_22_small")?;
        let reboot_steps = parse_reboot_steps(&input)?;
        let volume = puzzle_one(&reboot_steps);

        assert_eq!(590784, volume);

        Ok(())
    }

    #[test]
    fn puzzle_two_medium() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_22_medium")?;
        let reboot_steps = parse_reboot_steps(&input)?;
        let volume = puzzle_two(&reboot_steps);

        assert_eq!(2758514936282235, volume);

        Ok(())
    }

    #[test]
    fn overlap_none_right() -> Result<(), Box<dyn Error>> {
        let a = 0..=5;
        let b = 6..=7;
        let overlap = overlap(&a, &b);

        assert_eq!(None, overlap);

        Ok(())
    }

    #[test]
    fn overlap_none_left() -> Result<(), Box<dyn Error>> {
        let a = 0..=5;
        let b = -4..=-1;
        let overlap = overlap(&a, &b);

        assert_eq!(None, overlap);

        Ok(())
    }

    #[test]
    fn overlap_a_within_b() -> Result<(), Box<dyn Error>> {
        let a = 0..=5;
        let b = -4..=5;
        let overlap = overlap(&a, &b);

        assert_eq!(Some(0..=5), overlap);

        Ok(())
    }

    #[test]
    fn overlap_b_within_a() -> Result<(), Box<dyn Error>> {
        let a = -6..=5;
        let b = -6..=-4;
        let overlap = overlap(&a, &b);

        assert_eq!(Some(-6..=-4), overlap);

        Ok(())
    }

    #[test]
    fn overlap_right_of_a() -> Result<(), Box<dyn Error>> {
        let a = -6..=5;
        let b = 3..=8;
        let overlap = overlap(&a, &b);

        assert_eq!(Some(3..=5), overlap);

        Ok(())
    }

    #[test]
    fn overlap_right_of_b() -> Result<(), Box<dyn Error>> {
        let a = 8..=10;
        let b = 3..=8;
        let overlap = overlap(&a, &b);

        assert_eq!(Some(8..=8), overlap);

        Ok(())
    }
}
