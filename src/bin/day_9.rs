use std::collections::{HashSet, VecDeque};
use advent_of_code_2021::read_puzzle_input;
use std::error::Error;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(9)?;
    let height_map = HeightMap::from_str(&input)?;
    let risk_level_sum = puzzle_one(&height_map);
    println!("{}", risk_level_sum);
    let basin_product = puzzle_two(&height_map);
    println!("{}", basin_product);

    Ok(())
}

fn puzzle_one(height_map: &HeightMap) -> u32 {
    (0..height_map.rows as i64)
        .map(|y| {
            (0..height_map.columns as i64)
                .filter_map(|x| height_map.risk_level((x, y)))
                .sum::<u32>()
        })
        .sum()
}

fn puzzle_two(height_map: &HeightMap) -> u32 {
    let mut basin_sizes = height_map.basin_sizes();
    basin_sizes.sort_unstable();

    basin_sizes.into_iter().rev().take(3).product()
}

#[derive(Debug, Clone)]
struct HeightMap {
    columns: usize,
    rows: usize,
    points: Vec<u32>,
}

impl HeightMap {
    fn basin_sizes(&self) -> Vec<u32> {
        let mut basin_sizes = Vec::new();

        for (low_x, low_y) in self.low_points() {
            let mut size = 0;
            let mut queue = VecDeque::new();
            let mut enqueued_set = HashSet::new();
            queue.push_back((low_x, low_y));
            enqueued_set.insert((low_x, low_y));

            while let Some((x, y)) = queue.pop_front() {
                size += 1;

                for neighbour in self.neighbours((x, y)) {
                    if self.get(neighbour) == Some(&9) || enqueued_set.contains(&neighbour) {
                        continue;
                    }

                    queue.push_back(neighbour);
                    enqueued_set.insert(neighbour);
                }
            }

            basin_sizes.push(size);
        }

        basin_sizes
    }

    fn low_points(&self) -> Vec<(i64, i64)> {
        (0..self.rows as i64)
            .map(|y| {
                (0..self.columns as i64).filter_map(move |x| self.risk_level((x, y)).map(|_| (x, y)))
            })
            .flatten()
            .collect()
    }

    fn risk_level(&self, (column, row): (i64, i64)) -> Option<u32> {
        let height = *self.get((column, row))?;
        let is_low_point = self
            .neighbour_values((column, row))
            .all(|neighbour_height| *neighbour_height > height);

        if !is_low_point {
            return None;
        }

        Some(height + 1)
    }

    fn neighbours(&self, (column, row): (i64, i64)) -> impl Iterator<Item = (i64, i64)> + '_ {
        [
            (column, row - 1),
            (column + 1, row),
            (column, row + 1),
            (column - 1, row),
        ]
        .into_iter()
        .filter(|(x, y)| self.get((*x, *y)).is_some())
    }

    fn neighbour_values(&self, (column, row): (i64, i64)) -> impl Iterator<Item = &u32> {
        [
            (column, row - 1),
            (column + 1, row),
            (column, row + 1),
            (column - 1, row),
        ]
            .into_iter()
            .filter_map(|(x, y)| self.get((x, y)))
    }

    fn get(&self, (column, row): (i64, i64)) -> Option<&u32> {
        let (x_index, y_index) = (usize::try_from(column).ok()?, usize::try_from(row).ok()?);
        if x_index >= self.columns || y_index >= self.rows {
            return None;
        }

        let index = self.columns * y_index + x_index;
        self.points.get(index)
    }
}

impl FromStr for HeightMap {
    type Err = Box<dyn Error>;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut points = Vec::new();
        let mut columns = None;
        let mut rows = 0;

        for line in input.lines() {
            rows += 1;

            if *columns.get_or_insert(line.len()) != line.len() {
                return Err("Columns do not have equal length!".into());
            }

            for digit in line
                .chars()
                .map(|digit_char| digit_char.to_digit(10).unwrap())
            {
                points.push(digit);
            }
        }

        let columns = columns.unwrap_or(0);

        Ok(HeightMap {
            columns,
            rows,
            points,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, HeightMap, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;
    use std::str::FromStr;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_9_small")?;
        let height_map = HeightMap::from_str(&input)?;
        let risk_level_sum = puzzle_one(&height_map);

        assert_eq!(15, risk_level_sum);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_9_small")?;
        let height_map = HeightMap::from_str(&input)?;
        let basin_product = puzzle_two(&height_map);

        assert_eq!(1134, basin_product);

        Ok(())
    }
}
