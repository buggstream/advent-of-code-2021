use advent_of_code_2021::{read_puzzle_input, ParseList};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(1)?;
    let numbers = i64::parse_list(&input)?;
    let height_increases = puzzle_one(&numbers);

    println!("{:?}", height_increases);

    let sum_increases = puzzle_two(&numbers);

    println!("{}", sum_increases);

    Ok(())
}

fn puzzle_one(measurements: &[i64]) -> usize {
    let measurements = Vec::from(measurements);
    measurements
        .windows(2)
        .filter(|window_array| {
            let (left, right) = match window_array {
                [left, right] => (*left, *right),
                _ => unreachable!(),
            };
            right > left
        })
        .count()
}

fn puzzle_two(measurements: &[i64]) -> usize {
    let measurements = Vec::from(measurements);
    let sums: Vec<i64> = measurements
        .windows(3)
        .map(|window_array| window_array.iter().sum())
        .collect();
    sums.windows(2)
        .filter(|window_array| {
            let (left, right) = match window_array {
                [left, right] => (*left, *right),
                _ => unreachable!(),
            };
            right > left
        })
        .count()
}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, puzzle_two};
    use advent_of_code_2021::ParseList;
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_1_small")?;
        let numbers = i64::parse_list(&input)?;
        let height_increases = puzzle_one(&numbers);

        assert_eq!(7, height_increases);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_1_small")?;
        let numbers = i64::parse_list(&input)?;
        let sum_increases = puzzle_two(&numbers);

        assert_eq!(5, sum_increases);

        Ok(())
    }
}
