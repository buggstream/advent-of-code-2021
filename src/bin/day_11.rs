use advent_of_code_2021::read_puzzle_input;
use std::collections::VecDeque;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(11)?;
    let grid = OctopusGrid::from_str(&input)?;
    let flash_count = puzzle_one(grid.clone(), 100);
    println!("{}", flash_count);
    let steps = puzzle_two(grid);
    println!("{}", steps);

    Ok(())
}

fn puzzle_one(mut grid: OctopusGrid, steps: u64) -> u64 {
    (0..steps).map(|_| grid.simulate_step()).sum()
}

fn puzzle_two(mut grid: OctopusGrid) -> u64 {
    let mut step = 1;

    while grid.simulate_step() != 100 {
        step += 1;
    }

    step
}

#[derive(Debug, Clone)]
struct OctopusGrid {
    energy_levels: [[u8; 10]; 10],
}

impl OctopusGrid {
    fn simulate_step(&mut self) -> u64 {
        let mut flash_queue = VecDeque::new();

        for x in 0..10 {
            for y in 0..10 {
                self.energy_levels[x][y] += 1;

                if self.energy_levels[x][y] == 10 {
                    flash_queue.push_back((x, y));
                    self.energy_levels[x][y] = 0;
                }
            }
        }

        let mut flashed_locations = Vec::new();

        while let Some(location) = flash_queue.pop_front() {
            flashed_locations.push(location);

            for neighbour @ (neighbour_x, neighbour_y) in self.neighbours(location) {
                self.energy_levels[neighbour_x][neighbour_y] += 1;

                if self.energy_levels[neighbour_x][neighbour_y] == 10 {
                    flash_queue.push_back(neighbour);
                    self.energy_levels[neighbour_x][neighbour_y] = 0;
                }
            }
        }

        let flash_count = flashed_locations.len() as u64;

        for (x, y) in flashed_locations {
            self.energy_levels[x][y] = 0;
        }

        flash_count
    }

    fn neighbours<'a, 'b>(
        &'a self,
        (x, y): (usize, usize),
    ) -> impl Iterator<Item = (usize, usize)> + 'b {
        let (x, y) = (x as i32, y as i32);
        [
            (x, y - 1),
            (x + 1, y - 1),
            (x + 1, y),
            (x + 1, y + 1),
            (x, y + 1),
            (x - 1, y + 1),
            (x - 1, y),
            (x - 1, y - 1),
        ]
        .into_iter()
        .filter(|(x, y)| *x >= 0 && *x < 10 && *y >= 0 && *y < 10)
        .map(|(x, y)| (x as usize, y as usize))
    }
}

impl Display for OctopusGrid {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for y in 0..10 {
            for x in 0..10 {
                write!(f, "{}", self.energy_levels[x][y])?;
            }
            writeln!(f)?;
        }

        Ok(())
    }
}

impl FromStr for OctopusGrid {
    type Err = Box<dyn Error>;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut energy_levels = [[0; 10]; 10];

        for (y_index, line) in input.lines().enumerate() {
            if y_index >= 10 {
                return Err("There can't be more than 10 rows!".into());
            }

            for (x_index, digit_char) in line.chars().enumerate() {
                if x_index >= 10 {
                    return Err("There can't be more than 10 columns!".into());
                }

                let digit = digit_char.to_digit(10).ok_or("Invalid digit!")?;
                energy_levels[x_index][y_index] = digit as u8;
            }
        }

        Ok(OctopusGrid { energy_levels })
    }
}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, OctopusGrid, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;
    use std::str::FromStr;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_11_small")?;
        let grid = OctopusGrid::from_str(&input)?;
        let flash_count = puzzle_one(grid, 100);

        assert_eq!(1656, flash_count);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_11_small")?;
        let grid = OctopusGrid::from_str(&input)?;
        let step = puzzle_two(grid);

        assert_eq!(195, step);

        Ok(())
    }
}
