use advent_of_code_2021::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::cmp::max;
use std::error::Error;
use std::ops::RangeInclusive;
use std::str::FromStr;

lazy_static! {
    static ref TARGET_AREA_FORMAT: Regex =
        Regex::new(r"target area: x=(-?\d+)\.\.(-?\d+), y=(-?\d+)\.\.(-?\d+)").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(17)?;
    let target_area = parse_target_area(&input)?;
    let max_height = puzzle_one(&target_area);
    println!("{:?}", max_height);
    let unique_velocities = puzzle_two(&target_area);
    println!("{:?}", unique_velocities);

    Ok(())
}

fn puzzle_one((range_x, range_y): &TargetArea) -> Option<i64> {
    if *range_y.start() >= 0 || *range_y.end() >= 0 {
        return None;
    }

    let upper_bound_y = -*range_y.start();

    for start_velocity_y in (0..upper_bound_y).rev() {
        let mut steps_within_y_range = Vec::new();
        let mut position_y = -(start_velocity_y + 1);
        let mut velocity_y = -(start_velocity_y + 2);
        let mut current_step = (start_velocity_y + 1) * 2;

        while position_y >= *range_y.start() {
            if position_y <= *range_y.end() {
                steps_within_y_range.push(current_step);
            }

            position_y += velocity_y;
            velocity_y -= 1;
            current_step += 1;
        }

        for start_velocity_x in 0..=*range_x.end() {
            let max_x = gauss(start_velocity_x);

            if max_x < *range_x.start() {
                continue;
            }

            for step in steps_within_y_range.iter().copied() {
                let n = max(0, start_velocity_x - step);
                let position_x = max_x - gauss(n);

                if range_x.contains(&position_x) {
                    return Some(gauss(start_velocity_y));
                }
            }
        }
    }

    None
}

fn puzzle_two((range_x, range_y): &TargetArea) -> Option<i64> {
    if *range_y.start() >= 0 || *range_y.end() >= 0 {
        return None;
    }

    let mut unique_velocities = 0;
    let lower_bound_y = *range_y.start();
    let upper_bound_y = -*range_y.start();

    for start_velocity_y in lower_bound_y..upper_bound_y {
        let mut steps_within_y_range = Vec::new();

        let (mut position_y, mut velocity_y, mut current_step) = if start_velocity_y > 0 {
            (
                -(start_velocity_y + 1),
                -(start_velocity_y + 2),
                (start_velocity_y + 1) * 2,
            )
        } else {
            (0, start_velocity_y, 0)
        };

        while position_y >= *range_y.start() {
            if position_y <= *range_y.end() {
                steps_within_y_range.push(current_step);
            }

            position_y += velocity_y;
            velocity_y -= 1;
            current_step += 1;
        }

        'next_velocity_x: for start_velocity_x in 0..=*range_x.end() {
            let max_x = gauss(start_velocity_x);

            if max_x < *range_x.start() {
                continue;
            }

            for step in steps_within_y_range.iter().copied() {
                let n = max(0, start_velocity_x - step);
                let position_x = max_x - gauss(n);

                if range_x.contains(&position_x) {
                    unique_velocities += 1;
                    continue 'next_velocity_x;
                }
            }
        }
    }

    Some(unique_velocities)
}

#[inline]
fn gauss(n: i64) -> i64 {
    n * (n + 1) / 2
}

fn parse_target_area(input: &str) -> Result<TargetArea, Box<dyn Error>> {
    let captures = TARGET_AREA_FORMAT
        .captures(input)
        .ok_or("Could not parse target area!")?;

    let lower_x = i64::from_str(&captures[1])?;
    let upper_x = i64::from_str(&captures[2])?;
    let lower_y = i64::from_str(&captures[3])?;
    let upper_y = i64::from_str(&captures[4])?;

    Ok((lower_x..=upper_x, lower_y..=upper_y))
}

type TargetArea = (RangeInclusive<i64>, RangeInclusive<i64>);

#[cfg(test)]
mod test {
    use crate::{parse_target_area, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_17_small")?;
        let target_area = parse_target_area(&input)?;
        let max_height = puzzle_one(&target_area);

        assert_eq!(Some(45), max_height);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_17_small")?;
        let target_area = parse_target_area(&input)?;
        let unique_velocities = puzzle_two(&target_area);

        assert_eq!(Some(112), unique_velocities);

        Ok(())
    }
}
