use advent_of_code_2021::{read_puzzle_input, try_collect_array, AbsoluteDifference};
use lazy_static::lazy_static;
use regex::Regex;
use slotmap::{DefaultKey, SlotMap};
use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap, VecDeque};
use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use std::hash::Hash;

lazy_static! {
    static ref AMPHIPODS: Regex =
        Regex::new(r"(?m)^.{2}#([A-D])#([A-D])#([A-D])#([A-D])#.{0,2}$").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(23)?;
    let state = parse_folded_state(&input).unwrap();
    let least_energy = puzzle(state.clone());
    println!("{:?}", least_energy);
    let least_energy = puzzle(state.unfold());
    println!("{:?}", least_energy);

    Ok(())
}

fn puzzle<const N: usize>(start: BurrowState<N>) -> Option<u64>
where
    Position<N>: ValidPosition,
{
    let mut states = SlotMap::new();
    let start_key = states.insert(start);
    let mut state_map = HashMap::new();
    state_map.insert(start, start_key);
    let mut cache = HashMap::new();

    let mut previous_map = HashMap::new();
    previous_map.insert(start_key, start_key);
    let mut cost_map = HashMap::new();
    cost_map.insert(start_key, 0);
    let mut frontier = BinaryHeap::new();
    frontier.push(FrontierEntry::new(
        states[start_key].heuristic(),
        start_key,
        start_key,
    ));

    while let Some(FrontierEntry {
        current_key,
        previous_key,
        ..
    }) = frontier.pop()
    {
        if previous_map[&current_key] != previous_key {
            continue;
        }

        if states[current_key].is_organized() {
            return Some(cost_map[&current_key]);
        }

        for (next_state, movement_cost) in states[current_key].next_states(&mut cache) {
            let next_state_key = *state_map
                .entry(next_state)
                .or_insert_with(|| states.insert(next_state));
            let new_cost = cost_map[&current_key] + movement_cost;

            if !cost_map.contains_key(&next_state_key) || new_cost < cost_map[&next_state_key] {
                previous_map.insert(next_state_key, current_key);
                cost_map.insert(next_state_key, new_cost);
                let new_priority = new_cost + states[next_state_key].heuristic();
                frontier.push(FrontierEntry::new(
                    new_priority,
                    next_state_key,
                    current_key,
                ));
            }
        }
    }

    None
}

fn parse_folded_state(input: &str) -> Option<BurrowState<8>> {
    let amphipod_iter = AMPHIPODS
        .captures_iter(input)
        .map(|captures| {
            (1..5).map(move |capture_index| match &captures[capture_index] {
                "A" => Amphipod::Amber,
                "B" => Amphipod::Bronze,
                "C" => Amphipod::Copper,
                "D" => Amphipod::Desert,
                _ => unreachable!("Regex should capture only letters A, B, C & D."),
            })
        })
        .flatten()
        .enumerate()
        .map(|(index, amphipod)| (Position((8 + index) as u8), amphipod));

    let amphipods = try_collect_array(amphipod_iter)?;

    Some(BurrowState::start_state(amphipods))
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct BurrowState<const N: usize>
where
    Position<N>: ValidPosition,
{
    amphipods: [(Position<N>, Amphipod); N],
}

impl<const N: usize> BurrowState<N>
where
    Position<N>: ValidPosition,
{
    fn start_state(mut amphipods: [(Position<N>, Amphipod); N]) -> BurrowState<N> {
        amphipods.sort_unstable_by_key(|(pos, _)| *pos);

        BurrowState { amphipods }
    }

    fn next_states(
        &self,
        cache: &mut HashMap<PositionSet<N>, HashMap<Position<N>, PositionSet<N>>>,
    ) -> Vec<(Self, u64)> {
        let mut states = Vec::new();

        let position_set = PositionSet::from_iter(self.amphipods.iter().map(|(pos, _)| *pos));
        let connection_map = cache
            .entry(position_set)
            .or_insert_with(|| connected_areas(position_set));

        for (pos, amphipod) in self.amphipods.iter() {
            if pos.is_hallway() {
                if self.is_room_blocked(*amphipod) {
                    continue;
                }

                let destination = connection_map[pos]
                    .into_iter()
                    .filter(|connected_pos| connected_pos.is_destination_room(*amphipod))
                    .max();

                if let Some(destination) = destination {
                    let distance = pos.distance_to(destination);
                    let (new_state, moved_amphipod) = self.move_amphipod(*pos, destination);
                    let cost = moved_amphipod.movement_cost() * (distance as u64);
                    states.push((new_state, cost));
                }

                continue;
            }

            for connected_pos in connection_map[pos].into_iter() {
                if !connected_pos.is_hallway() {
                    continue;
                }

                let distance = pos.distance_to(connected_pos);
                let (new_state, moved_amphipod) = self.move_amphipod(*pos, connected_pos);
                let cost = moved_amphipod.movement_cost() * (distance as u64);
                states.push((new_state, cost));
            }
        }

        states
    }

    fn move_amphipod(&self, from: Position<N>, to: Position<N>) -> (Self, Amphipod) {
        let mut amphipods = self.amphipods;
        let (ref mut pos, amphipod) = *amphipods.iter_mut().find(|(pos, _)| *pos == from).unwrap();
        *pos = to;

        amphipods.sort_unstable_by_key(|(pos, _)| *pos);

        let new_state = BurrowState { amphipods };

        (new_state, amphipod)
    }

    fn heuristic(&self) -> u64 {
        let k = (N / 4 - 1) as u64;
        let triangular_number = k * (k + 1) / 2;
        const COST_OFFSET: u64 = Amphipod::Amber.movement_cost()
            + Amphipod::Bronze.movement_cost()
            + Amphipod::Copper.movement_cost()
            + Amphipod::Desert.movement_cost();

        let movement_cost: u64 = self
            .amphipods
            .iter()
            .map(|(pos, amphipod)| {
                (pos.distance_to_room(*amphipod) as u64) * amphipod.movement_cost()
            })
            .sum();

        let mut is_blocked = [false; 4];
        let mut blocked_heuristic = 0;

        for (pos, amphipod) in self.amphipods.iter().rev() {
            if pos.is_hallway() {
                continue;
            }

            let destination_index = match amphipod {
                Amphipod::Amber => 0,
                Amphipod::Bronze => 1,
                Amphipod::Copper => 2,
                Amphipod::Desert => 3,
            };

            let (x, y) = pos.coords();
            let current_index = (x / 2 - 1) as usize;

            if destination_index != current_index {
                is_blocked[current_index] = true;
            } else if is_blocked[current_index] {
                blocked_heuristic += amphipod.movement_cost() * (y + 1) as u64;
            }
        }

        movement_cost - (COST_OFFSET * triangular_number) + blocked_heuristic
    }

    fn is_organized(&self) -> bool {
        self.amphipods
            .iter()
            .all(|(pos, amphipod)| pos.is_destination_room(*amphipod))
    }

    fn is_room_blocked(&self, amphipod: Amphipod) -> bool {
        self.amphipods
            .iter()
            .any(|(pos, other)| *other != amphipod && pos.is_destination_room(amphipod))
    }
}

impl BurrowState<8> {
    fn convert_position(Position(position_index): Position<8>) -> Position<16> {
        if position_index < 12 {
            Position(position_index)
        } else {
            Position(position_index + 8)
        }
    }

    fn unfold(&self) -> BurrowState<16> {
        let mut amphipods = [(Position::default(), Amphipod::Amber); 16];

        for (index, (position, amphipod)) in self.amphipods.into_iter().enumerate() {
            amphipods[index] = (Self::convert_position(position), amphipod);
        }

        let inserted = [
            Amphipod::Desert,
            Amphipod::Copper,
            Amphipod::Bronze,
            Amphipod::Amber,
            Amphipod::Desert,
            Amphipod::Bronze,
            Amphipod::Amber,
            Amphipod::Copper,
        ];

        for (index, amphipod) in inserted.into_iter().enumerate() {
            amphipods[index + 8] = (Position((index as u8) + 12), amphipod);
        }

        amphipods.sort_unstable_by_key(|(pos, _)| *pos);

        BurrowState { amphipods }
    }
}

impl<const N: usize> Display for BurrowState<N>
where
    Position<N>: ValidPosition,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "#############\n#")?;

        let map: HashMap<_, _> = self
            .amphipods
            .iter()
            .map(|(pos, amphipod)| (*pos, amphipod.to_char()))
            .collect();

        for index in 1..3 {
            write!(f, "{}", map.get(&Position(index)).copied().unwrap_or('.'))?;
        }

        for index in 3..7 {
            write!(f, ".{}", map.get(&Position(index)).copied().unwrap_or('.'))?;
        }

        write!(f, "{}#\n###", map.get(&Position(7)).copied().unwrap_or('.'))?;

        for index in 8..12 {
            write!(f, "{}#", map.get(&Position(index)).copied().unwrap_or('.'))?;
        }

        write!(f, "##")?;

        for row in 0..((N - 4) / 4) as u8 {
            write!(f, "\n  #")?;

            for index in (12 + 4 * row)..(16 + 4 * row) {
                write!(f, "{}#", map.get(&Position(index)).copied().unwrap_or('.'))?;
            }

            write!(f, "  ")?;
        }

        write!(f, "\n  #########")?;

        Ok(())
    }
}

#[derive(Debug, Eq, PartialEq, Hash)]
struct FrontierEntry {
    current_key: DefaultKey,
    previous_key: DefaultKey,
    priority: u64,
}

impl FrontierEntry {
    fn new(priority: u64, current_key: DefaultKey, previous_key: DefaultKey) -> FrontierEntry {
        FrontierEntry {
            current_key,
            previous_key,
            priority,
        }
    }
}

impl PartialOrd for FrontierEntry {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.priority.cmp(&other.priority).reverse())
    }
}

impl Ord for FrontierEntry {
    fn cmp(&self, other: &Self) -> Ordering {
        self.priority.cmp(&other.priority).reverse()
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
enum Amphipod {
    Amber,
    Bronze,
    Copper,
    Desert,
}

impl Amphipod {
    const fn movement_cost(&self) -> u64 {
        match self {
            Amphipod::Amber => 1,
            Amphipod::Bronze => 10,
            Amphipod::Copper => 100,
            Amphipod::Desert => 1000,
        }
    }

    const fn to_char(self) -> char {
        match self {
            Amphipod::Amber => 'A',
            Amphipod::Bronze => 'B',
            Amphipod::Copper => 'C',
            Amphipod::Desert => 'D',
        }
    }
}

impl Default for Amphipod {
    fn default() -> Self {
        Amphipod::Amber
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
struct Position<const N: usize>(u8);

trait ValidPosition {}

//######################################
//## 01 02 .. 03 .. 04 .. 05 .. 06 07 ##
//######## 08 ## 09 ## 10 ## 11 ########
//      ## 12 ## 13 ## 14 ## 15 ##
//      ##########################
impl ValidPosition for Position<8> {}

//######################################
//## 01 02 .. 03 .. 04 .. 05 .. 06 07 ##
//######## 08 ## 09 ## 10 ## 11 ########
//      ## 12 ## 13 ## 14 ## 15 ##
//      ## 16 ## 17 ## 18 ## 19 ##
//      ## 20 ## 21 ## 22 ## 23 ##
//      ##########################
impl ValidPosition for Position<16> {}

impl<const N: usize> Position<N>
where
    Self: ValidPosition,
{
    fn neighbours(&self) -> PositionSet<N> {
        match self.0 {
            1 => PositionSet(1 << 1),
            2 => PositionSet((1 << 0) | (1 << 2) | (1 << 7)),
            3 => PositionSet((1 << 1) | (1 << 3) | (1 << 7) | (1 << 8)),
            4 => PositionSet((1 << 2) | (1 << 4) | (1 << 8) | (1 << 9)),
            5 => PositionSet((1 << 3) | (1 << 5) | (1 << 9) | (1 << 10)),
            6 => PositionSet((1 << 4) | (1 << 6) | (1 << 10)),
            7 => PositionSet(1 << 5),
            8 => PositionSet((1 << 1) | (1 << 2) | (1 << 11)),
            9 => PositionSet((1 << 2) | (1 << 3) | (1 << 12)),
            10 => PositionSet((1 << 3) | (1 << 4) | (1 << 13)),
            11 => PositionSet((1 << 4) | (1 << 5) | (1 << 14)),
            index if index >= 12 => {
                if index < (N + 4) as u8 {
                    PositionSet((1 << (index - 5)) | (1 << (index + 3)))
                } else {
                    PositionSet(1 << (index - 5))
                }
            }
            _ => panic!("Invalid position index!"),
        }
    }

    fn distance_to(&self, other: Position<N>) -> u8 {
        let (to_x, to_y) = other.coords();
        let (from_x, from_y) = self.coords();

        let distance_x = from_x.diff_abs(to_x);
        let distance_y = if distance_x == 0 {
            from_y.diff_abs(to_y)
        } else {
            from_y.diff_abs(0) + to_y.diff_abs(0)
        };

        distance_x + distance_y
    }

    fn distance_to_room(&self, amphipod: Amphipod) -> u8 {
        let destination = match amphipod {
            Amphipod::Amber => Position((N + 4) as u8),
            Amphipod::Bronze => Position((N + 5) as u8),
            Amphipod::Copper => Position((N + 6) as u8),
            Amphipod::Desert => Position((N + 7) as u8),
        };

        self.distance_to(destination)
    }

    fn coords(&self) -> (u8, u8) {
        match self.0 {
            1 => (0, 0),
            2 => (1, 0),
            3 => (3, 0),
            4 => (5, 0),
            5 => (7, 0),
            6 => (9, 0),
            7 => (10, 0),
            index if index >= 8 => {
                let x = 2 + ((index - 8) % 4) * 2;
                let y = 1 + (index - 8) / 4;

                (x, y)
            }
            _ => panic!("Invalid position index!"),
        }
    }

    fn is_destination_room(&self, amphipod: Amphipod) -> bool {
        if self.is_hallway() {
            return false;
        }

        let actual_column = (self.0 - 8) % 4;
        let destination_column = match amphipod {
            Amphipod::Amber => 0,
            Amphipod::Bronze => 1,
            Amphipod::Copper => 2,
            Amphipod::Desert => 3,
        };

        actual_column == destination_column
    }

    fn is_hallway(&self) -> bool {
        self.0 < 8
    }
}

impl<const N: usize> Default for Position<N> {
    fn default() -> Self {
        Position::<N>(1)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct PositionSet<const N: usize>(u32)
where
    Position<N>: ValidPosition;

impl<const N: usize> PositionSet<N>
where
    Position<N>: ValidPosition,
{
    const fn new() -> Self {
        PositionSet(0)
    }

    const fn contains(&self, pos: Position<N>) -> bool {
        let mask = 1 << (pos.0 - 1);
        (self.0 & mask) > 0
    }

    fn insert(&mut self, pos: Position<N>) {
        self.0 |= 1 << (pos.0 - 1);
    }

    fn union(&mut self, other: Self) {
        self.0 |= other.0;
    }

    const fn reverse(&self) -> Self {
        PositionSet(self.0 ^ u32::MAX)
    }
}

impl<const N: usize> FromIterator<Position<N>> for PositionSet<N>
where
    Position<N>: ValidPosition,
{
    fn from_iter<T: IntoIterator<Item = Position<N>>>(iter: T) -> Self {
        let mut bits = 0;

        for pos in iter.into_iter() {
            bits |= 1 << (pos.0 - 1);
        }

        PositionSet(bits)
    }
}

impl<const N: usize> IntoIterator for PositionSet<N>
where
    Position<N>: ValidPosition,
{
    type Item = Position<N>;
    type IntoIter = IterPositionSet<N>;

    fn into_iter(self) -> Self::IntoIter {
        IterPositionSet::new(self)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct IterPositionSet<const N: usize>
where
    Position<N>: ValidPosition,
{
    position_set: PositionSet<N>,
    offset: u8,
}

impl<const N: usize> IterPositionSet<N>
where
    Position<N>: ValidPosition,
{
    const fn new(position_set: PositionSet<N>) -> Self {
        IterPositionSet {
            position_set,
            offset: position_set.0.trailing_zeros() as u8,
        }
    }
}

impl<const N: usize> Iterator for IterPositionSet<N>
where
    Position<N>: ValidPosition,
{
    type Item = Position<N>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.offset >= (N + 7) as u8 {
            return None;
        }

        self.offset += 1;
        let next_position = Position(self.offset);
        let shifted = self.position_set.0 >> self.offset;
        self.offset += shifted.trailing_zeros() as u8;

        Some(next_position)
    }
}

fn connected_areas<const N: usize>(
    position_set: PositionSet<N>,
) -> HashMap<Position<N>, PositionSet<N>>
where
    Position<N>: ValidPosition,
{
    let mut areas = Vec::new();
    let mut area_map = HashMap::new();

    for pos in position_set.reverse() {
        if area_map.contains_key(&pos) {
            continue;
        }

        let mut new_area = PositionSet::new();
        let mut to_explore = VecDeque::new();
        to_explore.push_back(pos);
        area_map.insert(pos, areas.len());

        while let Some(next_pos) = to_explore.pop_front() {
            new_area.insert(next_pos);

            for neighbour_pos in next_pos.neighbours() {
                if position_set.contains(neighbour_pos) || area_map.contains_key(&neighbour_pos) {
                    continue;
                }

                to_explore.push_back(neighbour_pos);
                area_map.insert(neighbour_pos, areas.len());
            }
        }

        areas.push(new_area);
    }

    let mut connected_map = HashMap::new();

    for pos in position_set {
        let mut connected_set = PositionSet::new();

        for neighbour_pos in pos.neighbours() {
            if let Some(&index) = area_map.get(&neighbour_pos) {
                connected_set.union(areas[index]);
            }
        }

        connected_map.insert(pos, connected_set);
    }

    connected_map
}

#[cfg(test)]
mod test {
    use crate::{parse_folded_state, puzzle};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_23_small")?;
        let state = parse_folded_state(&input).unwrap();
        let least_energy = puzzle(state);

        assert_eq!(Some(12521), least_energy);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_23_small")?;
        let state = parse_folded_state(&input).unwrap().unfold();
        let least_energy = puzzle(state);

        assert_eq!(Some(44169), least_energy);

        Ok(())
    }
}
