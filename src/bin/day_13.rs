use advent_of_code_2021::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{HashSet, VecDeque};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::str::FromStr;

lazy_static! {
    static ref FOLD_INSTRUCTION: Regex = Regex::new(r"(?m)^fold along ([x|y])=(\d+)$").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(13)?;
    let mut paper = TransparentPaper::from_str(&input)?;
    let visible_dots = puzzle_one(paper.clone());
    println!("{}", visible_dots);
    let display = puzzle_two(&mut paper);
    println!("{}", display);

    Ok(())
}

fn puzzle_one(mut paper: TransparentPaper) -> usize {
    paper.next_fold();
    paper.visible_dots()
}

fn puzzle_two(paper: &mut TransparentPaper) -> String {
    paper.fold_all();
    format!("{}", paper)
}

#[derive(Debug, Clone)]
struct TransparentPaper {
    dots: Vec<(u32, u32)>,
    folds: VecDeque<FoldInstruction>,
}

impl TransparentPaper {
    fn fold_all(&mut self) {
        while !self.folds.is_empty() {
            self.next_fold();
        }
    }

    #[allow(clippy::comparison_chain)]
    fn next_fold(&mut self) {
        match self.folds.pop_front() {
            None => {}
            Some(FoldInstruction::XAxis(fold_value)) => {
                for (x, _) in self.dots.iter_mut() {
                    if *x > fold_value {
                        let distance = *x - fold_value;
                        *x = fold_value - distance;
                    } else if *x == fold_value {
                        panic!("Cannot fold along axis with dots!");
                    }
                }
            }
            Some(FoldInstruction::YAxis(fold_value)) => {
                for (_, y) in self.dots.iter_mut() {
                    if *y > fold_value {
                        let distance = *y - fold_value;
                        *y = fold_value - distance;
                    } else if *y == fold_value {
                        panic!("Cannot fold along axis with dots!");
                    }
                }
            }
        }
    }

    fn visible_dots(&self) -> usize {
        self.dots.iter().copied().collect::<HashSet<_>>().len()
    }
}

impl Display for TransparentPaper {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let max_x = self.dots.iter().copied().map(|(x, _)| x).max().unwrap_or(0);
        let max_y = self.dots.iter().copied().map(|(_, y)| y).max().unwrap_or(0);

        let dot_set: HashSet<_> = self.dots.iter().copied().collect();

        for y in 0..max_y + 1 {
            for x in 0..max_x + 1 {
                if dot_set.contains(&(x, y)) {
                    write!(f, "#")?;
                } else {
                    write!(f, ".")?;
                }
            }

            writeln!(f)?;
        }

        Ok(())
    }
}

impl FromStr for TransparentPaper {
    type Err = Box<dyn Error>;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let (input_dots, input_folds) = match input.split_once("\n\n") {
            None => return Err("No separate dots and fold input detected.".into()),
            Some(inputs) => inputs,
        };

        let mut dots = Vec::new();

        for line in input_dots.lines() {
            let coord = match line.split_once(',') {
                None => return Err(format!("Invalid coordinates: {:?}", line).into()),
                Some((left_str, right_str)) => {
                    (u32::from_str(left_str)?, u32::from_str(right_str)?)
                }
            };

            dots.push(coord);
        }

        let mut folds = VecDeque::new();

        for captures in FOLD_INSTRUCTION.captures_iter(input_folds) {
            let fold_value = u32::from_str(&captures[2])?;
            let fold = match &captures[1] {
                "x" => FoldInstruction::XAxis(fold_value),
                "y" => FoldInstruction::YAxis(fold_value),
                _ => unreachable!(),
            };

            folds.push_back(fold);
        }

        Ok(TransparentPaper { dots, folds })
    }
}

#[derive(Debug, Clone)]
enum FoldInstruction {
    XAxis(u32),
    YAxis(u32),
}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, puzzle_two, TransparentPaper};
    use std::error::Error;
    use std::fs::read_to_string;
    use std::str::FromStr;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_13_small")?;
        let paper = TransparentPaper::from_str(&input)?;
        let visible_dots = puzzle_one(paper.clone());

        assert_eq!(17, visible_dots);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_13_small")?;
        let mut paper = TransparentPaper::from_str(&input)?;
        let display = puzzle_two(&mut paper);

        let expected = r"
#####
#...#
#...#
#...#
#####
        ".trim();

        assert_eq!(expected, display.trim());

        Ok(())
    }
}
