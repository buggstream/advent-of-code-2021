use advent_of_code_2021::read_puzzle_input;
use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(4)?;
    let (drawing_order, cards, mapping) = parse_bingo(&input);
    let best_score = puzzle_one(&drawing_order, cards.clone(), mapping.clone());
    println!("{:?}", best_score);
    let worst_score = puzzle_two(&drawing_order, cards, mapping);
    println!("{:?}", worst_score);

    Ok(())
}

fn puzzle_one(
    drawing_order: &[u32],
    mut cards: Vec<BingoCardScore>,
    mapping: HashMap<u32, Vec<CardReference>>,
) -> Option<u32> {
    for drawn_number in drawing_order.iter().copied() {
        let matching_numbers = match mapping.get(&drawn_number) {
            None => continue,
            Some(matching_numbers) => matching_numbers,
        };

        for (card_index, (column_index, row_index)) in matching_numbers.iter().copied() {
            let card = &mut cards[card_index];
            card.decrease_unmarked_sum(drawn_number);
            let column_score = card.mark_column(column_index);
            let row_score = card.mark_row(row_index);

            if column_score == 5 || row_score == 5 {
                return Some(card.unmarked_sum * drawn_number);
            }
        }
    }

    None
}

fn puzzle_two(
    drawing_order: &[u32],
    mut cards: Vec<BingoCardScore>,
    mapping: HashMap<u32, Vec<CardReference>>,
) -> Option<u32> {
    let mut uncompleted_cards = HashSet::<usize>::from_iter(0..cards.len());
    for drawn_number in drawing_order.iter().copied() {
        let matching_numbers = match mapping.get(&drawn_number) {
            None => continue,
            Some(matching_numbers) => matching_numbers,
        };

        for (card_index, (column_index, row_index)) in matching_numbers.iter().copied() {
            let card = &mut cards[card_index];
            card.decrease_unmarked_sum(drawn_number);
            let column_score = card.mark_column(column_index);
            let row_score = card.mark_row(row_index);

            if column_score == 5 || row_score == 5 {
                uncompleted_cards.remove(&card_index);

                if uncompleted_cards.is_empty() {
                    return Some(card.unmarked_sum * drawn_number);
                }
            }
        }
    }

    None
}

fn parse_bingo(
    input: &str,
) -> (
    Vec<u32>,
    Vec<BingoCardScore>,
    HashMap<u32, Vec<CardReference>>,
) {
    let mut line_iter = input.lines();
    let drawing_order: Vec<_> = line_iter
        .next()
        .unwrap()
        .split(',')
        .map(|number_str| u32::from_str(number_str).unwrap())
        .collect();

    line_iter.next();

    let mut bingo_scores = Vec::new();
    let mut mark_map = HashMap::new();
    let mut unmarked_sum = 0;
    let mut row_index = 0;

    for line in line_iter {
        if line.is_empty() {
            bingo_scores.push(BingoCardScore::new(unmarked_sum));
            unmarked_sum = 0;
            row_index = 0;
            continue;
        }

        for (number, column_index) in line
            .split_whitespace()
            .map(|number_str| u32::from_str(number_str).unwrap())
            .zip(0_u8..)
        {
            let number_mappings = mark_map.entry(number).or_insert_with(Vec::new);
            number_mappings.push((bingo_scores.len(), (column_index, row_index)));
            unmarked_sum += number;
        }

        row_index += 1;
    }

    bingo_scores.push(BingoCardScore::new(unmarked_sum));

    (drawing_order, bingo_scores, mark_map)
}

type CardReference = (usize, (u8, u8));

#[derive(Debug, Copy, Clone)]
struct BingoCardScore {
    unmarked_sum: u32,
    row_score: [u8; 5],
    column_score: [u8; 5],
}

impl BingoCardScore {
    fn new(unmarked_sum: u32) -> BingoCardScore {
        BingoCardScore {
            unmarked_sum,
            row_score: [0; 5],
            column_score: [0; 5],
        }
    }

    fn decrease_unmarked_sum(&mut self, marked_number: u32) {
        self.unmarked_sum -= marked_number;
    }

    fn mark_row(&mut self, row_index: impl Into<usize>) -> u8 {
        let index = row_index.into();
        self.row_score[index] += 1;
        self.row_score[index]
    }

    fn mark_column(&mut self, column_index: impl Into<usize>) -> u8 {
        let index = column_index.into();
        self.column_score[index] += 1;
        self.column_score[index]
    }
}

#[cfg(test)]
mod test {
    use crate::{parse_bingo, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_4_small")?;
        let (drawing_order, cards, mapping) = parse_bingo(&input);
        let score = puzzle_one(&drawing_order, cards.clone(), mapping.clone());

        assert_eq!(Some(4512), score);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_4_small")?;
        let (drawing_order, cards, mapping) = parse_bingo(&input);
        let score = puzzle_two(&drawing_order, cards.clone(), mapping.clone());

        assert_eq!(Some(1924), score);

        Ok(())
    }
}
