use advent_of_code_2021::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::cmp::max;
use std::collections::{HashMap, HashSet, VecDeque};
use std::error::Error;
use std::ops::{Add, Sub};
use std::str::FromStr;

lazy_static! {
    static ref SCANNER_FORMAT: Regex = Regex::new(r"--- scanner (\d+) ---").unwrap();
    static ref BEACON_POSITION: Regex = Regex::new(r"(?m)^(-?\d+),(-?\d+),(-?\d+)$").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(19)?;
    let scanners = parse_scanners(&input);
    let beacon_map = BeaconMap::create_map(scanners, 12).unwrap();
    let unique_beacons = puzzle_one(&beacon_map);
    println!("{}", unique_beacons);
    let max_scanner_distance = puzzle_two(&beacon_map);
    println!("{:?}", max_scanner_distance);

    Ok(())
}

fn puzzle_one(map: &BeaconMap) -> usize {
    map.beacon_len()
}

fn puzzle_two(map: &BeaconMap) -> Option<i32> {
    map.max_scanner_distance()
}

fn parse_scanners(input: &str) -> Vec<Scanner> {
    input
        .trim()
        .split("\n\n")
        .map(|scanner_group| {
            let (first_line, other_lines) = scanner_group.split_once('\n').unwrap();

            let scanner_captures = SCANNER_FORMAT.captures(first_line).unwrap();
            let id = u32::from_str(&scanner_captures[1]).unwrap();

            let beacons: Vec<_> = BEACON_POSITION
                .captures_iter(other_lines)
                .map(|captures| {
                    let x = i32::from_str(&captures[1]).unwrap();
                    let y = i32::from_str(&captures[2]).unwrap();
                    let z = i32::from_str(&captures[3]).unwrap();

                    Position { x, y, z }
                })
                .collect();

            Scanner { id, beacons }
        })
        .collect()
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct BeaconMap {
    scanner_positions: Vec<(u32, Position)>,
    aligned_beacons: Vec<Position>,
}

impl BeaconMap {
    fn create_map(scanners: Vec<Scanner>, minimum_overlap: usize) -> Option<BeaconMap> {
        let mut scanner_queue = VecDeque::from_iter(scanners.into_iter());
        let mut map = BeaconMap::from(scanner_queue.pop_front()?);

        while let Some(next_scanner) = scanner_queue.pop_front() {
            if let Some(unmerged_scanner) = map.merge(next_scanner, minimum_overlap) {
                scanner_queue.push_back(unmerged_scanner);
            }
        }

        Some(map)
    }

    fn merge(&mut self, scanner: Scanner, mut minimum_overlap: usize) -> Option<Scanner> {
        let mut axis_aligned_beacons = Vec::new();
        let mut best_overlap = HashSet::new();

        for beacons in scanner.rotations().into_iter() {
            let current_overlap = self.best_overlap(&beacons);

            if current_overlap.len() >= minimum_overlap {
                best_overlap = current_overlap;
                axis_aligned_beacons = beacons;
                minimum_overlap = best_overlap.len();
            }
        }

        let (base_self, base_other) = match best_overlap.into_iter().next() {
            None => return Some(scanner),
            Some((index_self, index_other)) => (
                self.aligned_beacons[index_self],
                axis_aligned_beacons[index_other],
            ),
        };

        let aligned_set: HashSet<_> = self.aligned_beacons.iter().copied().collect();

        for &offset in base_other.relative_to(&axis_aligned_beacons).keys() {
            let new_position = base_self + offset;

            if !aligned_set.contains(&new_position) {
                self.aligned_beacons.push(new_position);
            }
        }

        self.scanner_positions
            .push((scanner.id, base_self - base_other));

        None
    }

    /// Can be optimized by first checking if there are a minimum amount of overlapping distances.
    fn best_overlap(&self, beacons: &[Position]) -> HashSet<(usize, usize)> {
        let mut all_aligned = HashSet::new();
        let mut best_aligned = HashSet::new();

        for i in 0..self.aligned_beacons.len() {
            for j in 0..beacons.len() {
                if all_aligned.contains(&(i, j)) {
                    continue;
                }

                let base_self = self.aligned_beacons[i];
                let base_other = beacons[j];

                let relative_self = base_self.relative_to(&self.aligned_beacons);
                let relative_other = base_other.relative_to(beacons);

                let mut aligned = HashSet::new();

                for (relative_pos, index_other) in relative_other.into_iter() {
                    if let Some(&index_self) = relative_self.get(&relative_pos) {
                        aligned.insert((index_self, index_other));
                    }
                }

                all_aligned.extend(aligned.iter().copied());

                if aligned.len() > best_aligned.len() {
                    best_aligned = aligned;
                }
            }
        }

        best_aligned
    }

    fn max_scanner_distance(&self) -> Option<i32> {
        if self.scanner_positions.is_empty() {
            return None;
        }

        let mut max_distance = 0;

        for (index, (_, from)) in self.scanner_positions.iter().enumerate() {
            for (_, to) in self.scanner_positions[index + 1..].iter() {
                max_distance = max(max_distance, from.manhattan_distance(to));
            }
        }

        Some(max_distance)
    }

    fn beacon_len(&self) -> usize {
        self.aligned_beacons.len()
    }
}

impl From<Scanner> for BeaconMap {
    fn from(scanner: Scanner) -> Self {
        BeaconMap {
            scanner_positions: vec![(scanner.id, Position::default())],
            aligned_beacons: scanner.beacons,
        }
    }
}

#[derive(Debug, Clone)]
struct Scanner {
    id: u32,
    beacons: Vec<Position>,
}

impl Scanner {
    fn rotations(&self) -> [Vec<Position>; 24] {
        let mut rotation_array: [Vec<Position>; 24] = Default::default();

        for base in self.beacons.iter() {
            for (index, rotated) in base.rotations().into_iter().enumerate() {
                rotation_array[index].push(rotated);
            }
        }

        rotation_array
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct Position {
    x: i32,
    y: i32,
    z: i32,
}

impl Position {
    fn new(x: i32, y: i32, z: i32) -> Position {
        Position { x, y, z }
    }

    fn relative_to(&self, positions: &[Position]) -> HashMap<Position, usize> {
        positions
            .iter()
            .enumerate()
            .map(|(index, rhs)| (*rhs - *self, index))
            .collect()
    }

    fn rotations(&self) -> [Position; 24] {
        // Implementation based on: https://stackoverflow.com/a/16467849/15228751
        let mut positions = [Position::default(); 24];
        let mut current = *self;
        let mut index = 0;

        for _ in 0..2 {
            for _ in 0..3 {
                current = current.roll();
                positions[index] = current;
                index += 1;

                for _ in 0..3 {
                    current = current.turn();
                    positions[index] = current;
                    index += 1;
                }
            }

            current = current.roll().turn().roll();
        }

        positions
    }

    fn roll(&self) -> Position {
        Position {
            x: self.x,
            y: self.z,
            z: -self.y,
        }
    }

    fn turn(&self) -> Position {
        Position {
            x: -self.y,
            y: self.x,
            z: self.z,
        }
    }

    fn manhattan_distance(&self, other: &Position) -> i32 {
        (self.x - other.x).abs() + (self.y - other.y).abs() + (self.z - other.z).abs()
    }
}

impl Add for Position {
    type Output = Position;

    fn add(self, rhs: Self) -> Self::Output {
        Position::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl Sub for Position {
    type Output = Position;

    fn sub(self, rhs: Self) -> Self::Output {
        Position::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}

impl Default for Position {
    fn default() -> Self {
        Position { x: 0, y: 0, z: 0 }
    }
}

#[cfg(test)]
mod test {
    use crate::{parse_scanners, puzzle_one, puzzle_two, BeaconMap, Position};
    use std::collections::HashSet;
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn merge() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_19_overlap")?;
        let scanners = parse_scanners(&input);
        let mut beacon_map = BeaconMap::from(scanners[0].clone());
        beacon_map.merge(scanners[1].clone(), 3);

        let expected_scanner_positions: HashSet<_> =
            [(0, Position::default()), (1, Position::new(4, -4, 0))]
                .into_iter()
                .collect();
        let expected_beacons: HashSet<_> = [
            Position::new(4, 2, 0),
            Position::new(-2, 0, 0),
            Position::new(5, -1, 0),
            Position::new(1, -2, 0),
            Position::new(-2, -3, 0),
            Position::new(8, -2, 0),
            Position::new(6, -5, 0),
        ]
        .into_iter()
        .collect();

        let BeaconMap {
            scanner_positions: positions,
            aligned_beacons: beacons,
        } = beacon_map;

        assert_eq!(expected_scanner_positions, positions.into_iter().collect());
        assert_eq!(expected_beacons, beacons.into_iter().collect());

        Ok(())
    }

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_19_small")?;
        let scanners = parse_scanners(&input);
        let beacon_map = BeaconMap::create_map(scanners, 12).unwrap();
        let unique_beacons = puzzle_one(&beacon_map);

        assert_eq!(79, unique_beacons);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_19_small")?;
        let scanners = parse_scanners(&input);
        let beacon_map = BeaconMap::create_map(scanners, 12).unwrap();
        let max_distance = puzzle_two(&beacon_map);

        assert_eq!(Some(3621), max_distance);

        Ok(())
    }
}
