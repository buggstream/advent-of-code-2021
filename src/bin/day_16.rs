use crate::Packet::{Literal, Operator};
use advent_of_code_2021::read_puzzle_input;
use std::collections::VecDeque;
use std::error::Error;
use std::ops::Range;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(16)?;
    let binary_data = parse_binary_data(&input)?;
    let mut bit_view = BitView::from(&binary_data[..]);
    let packet = Packet::from(&mut bit_view);
    let version_sum = puzzle_one(&packet);
    println!("{}", version_sum);
    let packet_value = packet.evaluate();
    println!("{}", packet_value);

    Ok(())
}

fn puzzle_one(packet: &Packet) -> u64 {
    let mut packet_queue = VecDeque::new();
    packet_queue.push_back(packet);
    let mut version_sum = 0;

    while let Some(popped_packet) = packet_queue.pop_front() {
        match popped_packet {
            Packet::Literal { version, .. } => version_sum += *version as u64,
            Packet::Operator {
                version,
                sub_packets,
                ..
            } => {
                version_sum += *version as u64;
                packet_queue.extend(sub_packets.iter());
            }
        }
    }

    version_sum
}

fn parse_binary_data(input: &str) -> Result<Vec<u8>, Box<dyn Error>> {
    let mut binary_data = Vec::new();
    let mut active_range = input.trim();

    while active_range.len() > 1 {
        let hex_byte = u8::from_str_radix(&active_range[..2], 16)?;
        binary_data.push(hex_byte);

        active_range = &active_range[2..];
    }

    Ok(binary_data)
}

#[derive(Debug, Clone, PartialEq)]
enum Packet {
    Literal {
        version: u8,
        value: u64,
    },
    Operator {
        version: u8,
        operation_type: OperationType,
        sub_packets: Vec<Packet>,
    },
}

impl Packet {
    fn evaluate(&self) -> u64 {
        match self {
            Packet::Literal { value, .. } => *value,
            Packet::Operator {
                operation_type,
                sub_packets,
                ..
            } => match (*operation_type, &sub_packets[..]) {
                (OperationType::Sum, packets) => {
                    packets.iter().map(|packet| packet.evaluate()).sum()
                }
                (OperationType::Product, packets) => {
                    packets.iter().map(|packet| packet.evaluate()).product()
                }
                (OperationType::Minimum, packets @ &[_, ..]) => packets
                    .iter()
                    .map(|packet| packet.evaluate())
                    .min()
                    .unwrap(),
                (OperationType::Maximum, packets @ &[_, ..]) => packets
                    .iter()
                    .map(|packet| packet.evaluate())
                    .max()
                    .unwrap(),
                (OperationType::GreaterThan, &[ref left, ref right]) => {
                    (left.evaluate() > right.evaluate()) as u64
                }
                (OperationType::LessThan, &[ref left, ref right]) => {
                    (left.evaluate() < right.evaluate()) as u64
                }
                (OperationType::EqualTo, &[ref left, ref right]) => {
                    (left.evaluate() == right.evaluate()) as u64
                }
                _ => panic!("Invalid operator packet: {:?}", self),
            },
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum OperationType {
    Sum,
    Product,
    Minimum,
    Maximum,
    GreaterThan,
    LessThan,
    EqualTo,
}

impl From<u8> for OperationType {
    fn from(type_id: u8) -> Self {
        match type_id {
            0 => OperationType::Sum,
            1 => OperationType::Product,
            2 => OperationType::Minimum,
            3 => OperationType::Maximum,
            5 => OperationType::GreaterThan,
            6 => OperationType::LessThan,
            7 => OperationType::EqualTo,
            _ => panic!("Invalid operation type id! {:?}", type_id),
        }
    }
}

impl<'a, 'b> From<&'b mut BitView<'a>> for Packet {
    fn from(bit_view: &mut BitView<'a>) -> Self {
        let version = bit_view.take(3).to_u8();
        let type_id = bit_view.take(3).to_u8();

        match type_id {
            4 => {
                let mut value = 0_u64;

                loop {
                    let group = bit_view.take(5).to_u8();
                    let group_value = (group & 0b1111) as u64;
                    value = (value << 4) | group_value;

                    if (group & 0b10000) == 0 {
                        break;
                    }
                }

                Literal { version, value }
            }
            _ => {
                let length_type_id = bit_view.take(1).to_bool();
                let mut sub_packets = Vec::new();

                if length_type_id {
                    let sub_packet_amount = bit_view.take(11).to_u64();

                    for _ in 0..sub_packet_amount {
                        let sub_packet = Packet::from(&mut *bit_view);
                        sub_packets.push(sub_packet);
                    }
                } else {
                    let sub_packets_length = bit_view.take(15).to_u64();
                    let mut sub_packets_view = bit_view.take(sub_packets_length as usize);

                    while sub_packets_view.len() > 0 {
                        let sub_packet = Packet::from(&mut sub_packets_view);
                        sub_packets.push(sub_packet);
                    }
                }

                let operation_type = OperationType::from(type_id);

                Operator {
                    version,
                    operation_type,
                    sub_packets,
                }
            }
        }
    }
}

#[derive(Debug, Clone)]
struct BitView<'a> {
    raw_data: &'a [u8],
    range: Range<usize>,
}

impl<'a> BitView<'a> {
    fn to_bool(&self) -> bool {
        assert_eq!(self.len(), 1);

        let index = self.range.start / 8;
        let offset = 7 - (self.range.start % 8);
        self.raw_data[index] & (1 << offset) > 0
    }

    fn to_u8(&self) -> u8 {
        assert!(self.len() > 0);
        assert!(self.len() <= 8);

        let start_index = self.range.start / 8;
        let end_index = (self.range.end - 1) / 8 + 1;

        let mut output = self.raw_data[start_index];
        let last_byte_length = (self.range.end - 1) % 8 + 1;
        let final_byte = self.raw_data[end_index - 1] >> (8 - last_byte_length);
        output = (output << (last_byte_length % 8)) | final_byte;

        let mask = u8::MAX >> (8 - self.len());

        output & mask
    }

    fn to_u64(&self) -> u64 {
        assert!(self.len() > 0);
        assert!(self.len() <= 64);

        let start_index = self.range.start / 8;
        let end_index = (self.range.end - 1) / 8 + 1;

        let mut output = 0;

        for byte in self.raw_data[start_index..end_index - 1].iter().copied() {
            output = (output << 8) | byte as u64;
        }

        let last_byte_length = (self.range.end - 1) % 8 + 1;
        let final_byte = self.raw_data[end_index - 1] >> (8 - last_byte_length);
        output = (output << last_byte_length) | final_byte as u64;

        let mask = u64::MAX >> (64 - self.len());

        output & mask
    }

    fn len(&self) -> usize {
        self.range.len()
    }

    fn take(&mut self, n: usize) -> BitView<'a> {
        assert!(n <= self.len());

        let taken_view = BitView {
            raw_data: self.raw_data,
            range: self.range.start..self.range.start + n,
        };

        self.range.start += n;

        taken_view
    }
}

impl<'a> From<&'a [u8]> for BitView<'a> {
    fn from(raw_data: &'a [u8]) -> Self {
        let bit_length = 8 * raw_data.len();
        BitView {
            raw_data,
            range: 0..bit_length,
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{parse_binary_data, puzzle_one, BitView, OperationType, Packet};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn parse_packet_small() -> Result<(), Box<dyn Error>> {
        let binary_data = parse_binary_data("D2FE28")?;
        let mut bit_view = BitView::from(&binary_data[..]);
        let packet = Packet::from(&mut bit_view);

        assert_eq!(
            Packet::Literal {
                version: 6,
                value: 2021
            },
            packet
        );

        Ok(())
    }

    #[test]
    fn parse_packet_medium_v1() -> Result<(), Box<dyn Error>> {
        let binary_data = parse_binary_data("38006F45291200")?;
        let mut bit_view = BitView::from(&binary_data[..]);
        let packet = Packet::from(&mut bit_view);

        let expected = Packet::Operator {
            version: 1,
            operation_type: OperationType::LessThan,
            sub_packets: vec![
                Packet::Literal {
                    version: 6,
                    value: 10,
                },
                Packet::Literal {
                    version: 2,
                    value: 20,
                },
            ],
        };

        assert_eq!(expected, packet);

        Ok(())
    }

    #[test]
    fn parse_packet_medium_v2() -> Result<(), Box<dyn Error>> {
        let binary_data = parse_binary_data("EE00D40C823060")?;
        let mut bit_view = BitView::from(&binary_data[..]);
        let packet = Packet::from(&mut bit_view);

        let expected = Packet::Operator {
            version: 7,
            operation_type: OperationType::Maximum,
            sub_packets: vec![
                Packet::Literal {
                    version: 2,
                    value: 1,
                },
                Packet::Literal {
                    version: 4,
                    value: 2,
                },
                Packet::Literal {
                    version: 1,
                    value: 3,
                },
            ],
        };

        assert_eq!(expected, packet);

        Ok(())
    }

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_16_small")?;
        let binary_data = parse_binary_data(&input)?;
        let mut bit_view = BitView::from(&binary_data[..]);
        let packet = Packet::from(&mut bit_view);
        let version_sum = puzzle_one(&packet);

        assert_eq!(31, version_sum);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let binary_data = parse_binary_data("9C0141080250320F1802104A08")?;
        let mut bit_view = BitView::from(&binary_data[..]);
        let packet = Packet::from(&mut bit_view);
        let packet_value = packet.evaluate();

        assert_eq!(1, packet_value);

        Ok(())
    }
}
