use advent_of_code_2021::read_puzzle_input;
use std::collections::VecDeque;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(10)?;
    let corrupted_score = puzzle_one(&input);
    println!("{}", corrupted_score);
    let autocomplete_score = puzzle_two(&input);
    println!("{}", autocomplete_score);

    Ok(())
}

fn puzzle_one(input: &str) -> u64 {
    let mut syntax_error_score = 0;

    for line in input.lines() {
        let mut stack = VecDeque::new();

        for bracket in line.chars() {
            match (stack.front().copied(), bracket) {
                (_, '(') | (_, '[') | (_, '{') | (_, '<') => stack.push_front(bracket),
                (Some('('), ')') | (Some('['), ']') | (Some('{'), '}') | (Some('<'), '>') => {
                    stack.pop_front();
                    continue;
                }
                (Some(_), ')') => {
                    stack.pop_front();
                    syntax_error_score += 3;
                    break;
                }
                (Some(_), ']') => {
                    stack.pop_front();
                    syntax_error_score += 57;
                    break;
                }
                (Some(_), '}') => {
                    stack.pop_front();
                    syntax_error_score += 1197;
                    break;
                }
                (Some(_), '>') => {
                    stack.pop_front();
                    syntax_error_score += 25137;
                    break;
                }
                _ => panic!("Invalid input!"),
            }
        }
    }

    syntax_error_score
}

fn puzzle_two(input: &str) -> u64 {
    let mut autocomplete_scores = Vec::new();

    'line: for line in input.lines() {
        let mut stack = VecDeque::new();

        for bracket in line.chars() {
            match (stack.front().copied(), bracket) {
                (_, '(') | (_, '[') | (_, '{') | (_, '<') => stack.push_front(bracket),
                (Some('('), ')') | (Some('['), ']') | (Some('{'), '}') | (Some('<'), '>') => {
                    stack.pop_front();
                    continue;
                }
                _ => continue 'line,
            }
        }

        let line_score = stack
            .drain(..)
            .fold(0, |acc, uncompleted_char| match uncompleted_char {
                '(' => acc * 5 + 1,
                '[' => acc * 5 + 2,
                '{' => acc * 5 + 3,
                '<' => acc * 5 + 4,
                _ => unreachable!(),
            });
        autocomplete_scores.push(line_score);
    }

    autocomplete_scores.sort_unstable();
    autocomplete_scores[autocomplete_scores.len() / 2]
}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_10_small")?;
        let corrupted_score = puzzle_one(&input);

        assert_eq!(26397, corrupted_score);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_10_small")?;
        let autocomplete_score = puzzle_two(&input);

        assert_eq!(288957, autocomplete_score);

        Ok(())
    }
}
