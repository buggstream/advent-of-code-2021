use advent_of_code_2021::read_puzzle_input;
use itertools::Itertools;
use std::collections::HashMap;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(14)?;
    let (template, insertion_rules) = parse_polymer(&input)?;
    let polymers = puzzle(template, &insertion_rules, 10);
    println!("{}", polymers);
    let polymers = puzzle(template, &insertion_rules, 40);
    println!("{}", polymers);

    Ok(())
}

fn puzzle(template: &str, pair_map: &HashMap<Pair, (Pair, Pair)>, steps: usize) -> u64 {
    if template.chars().count() < 2 {
        return 0;
    }

    let mut pairs = HashMap::new();

    for pair in template.chars().tuple_windows::<(_, _)>() {
        let pair_occurrence = pairs.entry(pair).or_insert(0_u64);
        *pair_occurrence += 1;
    }

    for _ in 0..steps {
        let mut new_pairs = HashMap::new();

        for (pair, occurrence) in pairs.into_iter() {
            let (left_pair, right_pair) = pair_map[&pair];
            let left_occurrence = new_pairs.entry(left_pair).or_insert(0_u64);
            *left_occurrence += occurrence;
            let right_occurrence = new_pairs.entry(right_pair).or_insert(0_u64);
            *right_occurrence += occurrence;
        }

        pairs = new_pairs;
    }

    let mut occurrences = HashMap::new();
    let first_char = template.chars().next().unwrap();
    occurrences.insert(first_char, 1);

    for ((_, letter), occurrence) in pairs.into_iter() {
        let letter_count = occurrences.entry(letter).or_insert(0_u64);
        *letter_count += occurrence;
    }

    *occurrences.values().max().unwrap() - *occurrences.values().min().unwrap()
}

#[allow(clippy::type_complexity)]
fn parse_polymer(input: &str) -> Result<(&str, HashMap<Pair, (Pair, Pair)>), Box<dyn Error>> {
    let (template_input, rule_input) = match input.split_once("\n\n") {
        None => return Err("No separate template and insertion rules detected!".into()),
        Some(inputs) => inputs,
    };

    let mut pair_map = HashMap::new();

    for line in rule_input.lines() {
        let (first_letter, inserted, second_letter) = match line.split_once(" -> ") {
            None => return Err("Invalid pair insertion rule formatting!".into()),
            Some((pair, inserted)) => {
                let mut chars = pair.chars();
                (
                    chars.next().ok_or("No first letter in pair!")?,
                    inserted.chars().next().ok_or("No inserted letter!")?,
                    chars.next().ok_or("No second letter in pair!")?,
                )
            }
        };

        pair_map.insert(
            (first_letter, second_letter),
            ((first_letter, inserted), (inserted, second_letter)),
        );
    }

    Ok((template_input.trim(), pair_map))
}

type Pair = (char, char);

#[cfg(test)]
mod test {
    use crate::{parse_polymer, puzzle};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_14_small")?;
        let (template, insertion_rules) = parse_polymer(&input)?;
        let polymers = puzzle(template, &insertion_rules, 10);

        assert_eq!(1588, polymers);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_14_small")?;
        let (template, insertion_rules) = parse_polymer(&input)?;
        let polymers = puzzle(template, &insertion_rules, 40);

        assert_eq!(2188189693529, polymers);

        Ok(())
    }
}
