use advent_of_code_2021::read_puzzle_input;
use petgraph::graph::{NodeIndex, UnGraph};
use std::collections::{HashMap, HashSet, VecDeque};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(12)?;
    let (graph, big_caves) = parse_graph(&input)?;
    let path_count = puzzle_one(&graph, &big_caves);
    println!("{}", path_count);
    let multi_path_count = puzzle_two(&graph, &big_caves);
    println!("{}", multi_path_count);

    Ok(())
}

fn puzzle_one(graph: &UnGraph<&str, ()>, big_caves: &HashSet<NodeIndex>) -> u64 {
    let mut path_count = 0;
    let start_node = graph
        .node_indices()
        .find(|node_index| graph[*node_index] == "start")
        .unwrap();
    let end_node = graph
        .node_indices()
        .find(|node_index| graph[*node_index] == "end")
        .unwrap();

    let mut state_stack = VecDeque::new();
    state_stack.push_back(ExplorationState::start_new(start_node));

    while let Some(mut state) = state_stack.pop_back() {
        if state.to_node == end_node {
            path_count += 1;
            continue;
        }

        if !big_caves.contains(&state.to_node) {
            state.explored.insert(state.to_node);
        }

        state.from_node = state.to_node;

        for neighbour in graph
            .neighbors(state.to_node)
            .filter(|node| !state.explored.contains(node))
        {
            let mut next_state = state.clone();
            next_state.to_node = neighbour;
            state_stack.push_back(next_state);
        }
    }

    path_count
}

fn puzzle_two(graph: &UnGraph<&str, ()>, big_caves: &HashSet<NodeIndex>) -> u64 {
    let mut path_count = 0;
    let start_node = graph
        .node_indices()
        .find(|node_index| graph[*node_index] == "start")
        .unwrap();
    let end_node = graph
        .node_indices()
        .find(|node_index| graph[*node_index] == "end")
        .unwrap();

    let mut state_stack = VecDeque::new();
    state_stack.push_back(MultiExplorationState::start_new(start_node));

    while let Some(mut state) = state_stack.pop_back() {
        if state.to_node == end_node {
            path_count += 1;
            continue;
        }

        if !big_caves.contains(&state.to_node) {
            if state.explored.contains(&state.to_node) {
                state.explore_twice_used = true;
            } else {
                state.explored.insert(state.to_node);
            }
        }

        state.from_node = state.to_node;

        for neighbour in graph.neighbors(state.to_node).filter(|node| {
            match (state.explored.contains(node), state.explore_twice_used) {
                (false, _) => true,
                (true, false) if *node != start_node => true,
                _ => false,
            }
        }) {
            let mut next_state = state.clone();
            next_state.to_node = neighbour;
            state_stack.push_back(next_state);
        }
    }

    path_count
}

#[allow(clippy::type_complexity)]
fn parse_graph(input: &str) -> Result<(UnGraph<&str, ()>, HashSet<NodeIndex>), Box<dyn Error>> {
    let mut graph = UnGraph::new_undirected();
    let mut nodes = HashMap::new();
    let mut big_caves = HashSet::new();

    for line in input.lines() {
        match line.split_once('-') {
            None => return Err(format!("Invalid edge: {}", line).into()),
            Some((from, to)) => {
                let from_node = *nodes.entry(from).or_insert_with(|| {
                    let new_node = graph.add_node(from);
                    if from.chars().all(|letter| letter.is_uppercase()) {
                        big_caves.insert(new_node);
                    }
                    new_node
                });
                let to_node = *nodes.entry(to).or_insert_with(|| {
                    let new_node = graph.add_node(to);
                    if to.chars().all(|letter| letter.is_uppercase()) {
                        big_caves.insert(new_node);
                    }
                    new_node
                });

                graph.add_edge(from_node, to_node, ());
            }
        }
    }

    Ok((graph, big_caves))
}

#[derive(Debug, Clone)]
struct ExplorationState {
    from_node: NodeIndex,
    to_node: NodeIndex,
    explored: HashSet<NodeIndex>,
}

impl ExplorationState {
    fn start_new(start_node: NodeIndex) -> ExplorationState {
        ExplorationState {
            from_node: start_node,
            to_node: start_node,
            explored: HashSet::new(),
        }
    }
}

#[derive(Debug, Clone)]
struct MultiExplorationState {
    from_node: NodeIndex,
    to_node: NodeIndex,
    explored: HashSet<NodeIndex>,
    explore_twice_used: bool,
}

impl MultiExplorationState {
    fn start_new(start_node: NodeIndex) -> MultiExplorationState {
        MultiExplorationState {
            from_node: start_node,
            to_node: start_node,
            explored: HashSet::new(),
            explore_twice_used: false,
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{parse_graph, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_12_small")?;
        let (graph, big_caves) = parse_graph(&input)?;
        let path_count = puzzle_one(&graph, &big_caves);

        assert_eq!(10, path_count);

        Ok(())
    }

    #[test]
    fn puzzle_one_medium() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_12_medium")?;
        let (graph, big_caves) = parse_graph(&input)?;
        let path_count = puzzle_one(&graph, &big_caves);

        assert_eq!(19, path_count);

        Ok(())
    }

    #[test]
    fn puzzle_one_large() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_12_large")?;
        let (graph, big_caves) = parse_graph(&input)?;
        let path_count = puzzle_one(&graph, &big_caves);

        assert_eq!(226, path_count);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_12_small")?;
        let (graph, big_caves) = parse_graph(&input)?;
        let path_count = puzzle_two(&graph, &big_caves);

        assert_eq!(36, path_count);

        Ok(())
    }

    #[test]
    fn puzzle_two_medium() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_12_medium")?;
        let (graph, big_caves) = parse_graph(&input)?;
        let path_count = puzzle_two(&graph, &big_caves);

        assert_eq!(103, path_count);

        Ok(())
    }

    #[test]
    fn puzzle_two_large() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_12_large")?;
        let (graph, big_caves) = parse_graph(&input)?;
        let path_count = puzzle_two(&graph, &big_caves);

        assert_eq!(3509, path_count);

        Ok(())
    }
}
