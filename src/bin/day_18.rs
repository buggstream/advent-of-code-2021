use advent_of_code_2021::read_puzzle_input;
use itertools::Itertools;
use std::cmp::max;
use std::collections::VecDeque;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::ops::Add;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(18)?;
    let snailfish_numbers = parse_snailfish_numbers(&input);
    let magnitude = puzzle_one(snailfish_numbers.clone());
    println!("{:?}", magnitude);
    let max_magnitude = puzzle_two(snailfish_numbers);
    println!("{:?}", max_magnitude);

    Ok(())
}

fn puzzle_one(snailfish_numbers: Vec<SnailfishNumber>) -> Option<i32> {
    snailfish_numbers
        .into_iter()
        .reduce(|left, right| left + right)
        .map(|number| number.magnitude())
}

fn puzzle_two(snailfish_numbers: Vec<SnailfishNumber>) -> Option<i32> {
    if snailfish_numbers.len() < 2 {
        return None;
    }

    let mut max_magnitude = 0;

    for left_index in 0..snailfish_numbers.len() {
        for right_index in 0..snailfish_numbers.len() {
            if left_index == right_index {
                continue;
            }

            let left = snailfish_numbers[left_index].clone();
            let right = snailfish_numbers[right_index].clone();
            max_magnitude = max(max_magnitude, (left + right).magnitude());
        }
    }

    Some(max_magnitude)
}

fn parse_snailfish_numbers(input: &str) -> Vec<SnailfishNumber> {
    input
        .lines()
        .map(|line| SnailfishNumber::from_str(line).unwrap())
        .collect()
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct SnailfishNumber {
    tokens: Vec<Token>,
}

impl SnailfishNumber {
    fn magnitude(&self) -> i32 {
        let mut stack = VecDeque::new();

        for token in self.tokens.iter() {
            match token {
                Token::Close => {
                    let right = stack.pop_back().unwrap();
                    let left = stack.pop_back().unwrap();

                    stack.push_back(3 * left + 2 * right);
                }
                Token::Number(number) => stack.push_back(*number),
                _ => {}
            }
        }

        stack.pop_back().unwrap()
    }

    fn reduce(&mut self) {
        loop {
            if self.try_explode() || self.try_split() {
                continue;
            }

            break;
        }
    }

    fn try_explode(&mut self) -> bool {
        let mut depth = 0;

        for index in 0..(self.tokens.len() - 3) {
            match self.tokens[index..(index + 4)] {
                [Token::Open, Token::Number(left), Token::Number(right), Token::Close]
                    if depth == 4 =>
                {
                    for search_index in (0..index).rev() {
                        if let Token::Number(caught_left) = &mut self.tokens[search_index] {
                            *caught_left += left;
                            break;
                        }
                    }

                    for search_index in index + 4..self.tokens.len() {
                        if let Token::Number(caught_right) = &mut self.tokens[search_index] {
                            *caught_right += right;
                            break;
                        }
                    }

                    self.tokens[index] = Token::Number(0);

                    for _ in 0..3 {
                        self.tokens.remove(index + 1);
                    }

                    return true;
                }
                [Token::Open, ..] => depth += 1,
                [Token::Close, ..] => depth -= 1,
                _ => {}
            }
        }

        false
    }

    fn try_split(&mut self) -> bool {
        for index in 0..self.tokens.len() {
            match self.tokens[index] {
                Token::Number(number) if number >= 10 => {
                    let left_number = number / 2;
                    let right_number = left_number + (number % 2);

                    self.tokens[index] = Token::Open;
                    self.tokens.insert(index + 1, Token::Number(left_number));
                    self.tokens.insert(index + 2, Token::Number(right_number));
                    self.tokens.insert(index + 3, Token::Close);

                    return true;
                }
                _ => {}
            }
        }

        false
    }
}

impl Add for SnailfishNumber {
    type Output = SnailfishNumber;

    fn add(self, rhs: Self) -> Self::Output {
        let mut tokens = vec![Token::Open];
        tokens.extend(self.tokens.into_iter());
        tokens.extend(rhs.tokens.into_iter());
        tokens.push(Token::Close);

        let mut sum = SnailfishNumber { tokens };
        sum.reduce();
        sum
    }
}

impl FromStr for SnailfishNumber {
    type Err = Box<dyn Error>;

    fn from_str(line: &str) -> Result<Self, Self::Err> {
        let tokens: Vec<_> = line
            .chars()
            .filter_map(|letter| match letter {
                '[' => Some(Token::Open),
                ']' => Some(Token::Close),
                digit if digit.is_digit(10) => {
                    Some(Token::Number(digit.to_digit(10).unwrap() as i32))
                }
                _ => None,
            })
            .collect();

        Ok(SnailfishNumber { tokens })
    }
}

impl Display for SnailfishNumber {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let string = self
            .tokens
            .iter()
            .map(|token| match token {
                Token::Open => "[".to_string(),
                Token::Close => "]".to_string(),
                Token::Number(number) => number.to_string(),
            })
            .join("");

        write!(f, "{}", string)?;

        Ok(())
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Token {
    Open,
    Close,
    Number(i32),
}

#[cfg(test)]
mod test {
    use crate::{parse_snailfish_numbers, puzzle_one, puzzle_two, SnailfishNumber};
    use std::error::Error;
    use std::fs::read_to_string;
    use std::str::FromStr;

    #[test]
    fn snailfish_number_addition() -> Result<(), Box<dyn Error>> {
        let lhs = SnailfishNumber::from_str("[[[[4,3],4],4],[7,[[8,4],9]]]")?;
        let rhs = SnailfishNumber::from_str("[1,1]")?;
        let expected = SnailfishNumber::from_str("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")?;

        assert_eq!(expected, lhs + rhs);

        Ok(())
    }

    #[test]
    fn snailfish_number_magnitude() -> Result<(), Box<dyn Error>> {
        let number =
            SnailfishNumber::from_str("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")?;

        assert_eq!(3488, number.magnitude());

        Ok(())
    }

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_18_small")?;
        let snailfish_numbers = parse_snailfish_numbers(&input);
        let magnitude = puzzle_one(snailfish_numbers);

        assert_eq!(Some(4140), magnitude);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_18_small")?;
        let snailfish_numbers = parse_snailfish_numbers(&input);
        let max_magnitude = puzzle_two(snailfish_numbers);

        assert_eq!(Some(3993), max_magnitude);

        Ok(())
    }
}
