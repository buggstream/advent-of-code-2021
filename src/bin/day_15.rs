use advent_of_code_2021::read_puzzle_input;
use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(15)?;
    let grid = RiskGrid::from_str(&input)?;
    let cost = grid.find_path((0, 0), (grid.width - 1, grid.height - 1));
    println!("{:?}", cost);
    let scaled_grid = grid.scale(5);
    let full_cost = scaled_grid.find_path((0, 0), (scaled_grid.width - 1, scaled_grid.height - 1));
    println!("{:?}", full_cost);

    Ok(())
}

#[derive(Debug, Clone)]
struct RiskGrid {
    width: usize,
    height: usize,
    risk_values: Vec<u32>,
}

impl RiskGrid {
    fn find_path(&self, start: (usize, usize), destination: (usize, usize)) -> Option<u32> {
        if self.get(start).is_none() || self.get(destination).is_none() {
            return None;
        }

        let mut previous_map = HashMap::new();
        previous_map.insert(start, start);
        let mut cost_map = HashMap::new();
        cost_map.insert(start, 0);
        let mut frontier = BinaryHeap::new();
        frontier.push(FrontierEntry(0, start, start));

        while let Some(FrontierEntry(_, current, previous)) = frontier.pop() {
            if previous_map[&current] != previous {
                continue;
            }

            if current == destination {
                return Some(cost_map[&current]);
            }

            for neighbour in self.neighbours(current) {
                let new_cost = cost_map[&current] + self.get(neighbour).unwrap();

                if !cost_map.contains_key(&neighbour) || new_cost < cost_map[&neighbour] {
                    previous_map.insert(neighbour, current);
                    cost_map.insert(neighbour, new_cost);
                    let new_priority = new_cost + self.heuristic(neighbour, destination);
                    frontier.push(FrontierEntry(new_priority, neighbour, current));
                }
            }
        }

        None
    }

    fn heuristic(&self, (x, y): (usize, usize), (dest_x, dest_y): (usize, usize)) -> u32 {
        let difference_x = (dest_x as i64 - x as i64).abs() as u32;
        let difference_y = (dest_y as i64 - y as i64).abs() as u32;
        difference_x + difference_y
    }

    fn neighbours(&self, (x, y): (usize, usize)) -> impl Iterator<Item = (usize, usize)> + '_ {
        let (x, y) = (x as i64, y as i64);
        [(x, y - 1), (x + 1, y), (x, y + 1), (x - 1, y)]
            .into_iter()
            .filter(|(x, y)| {
                *x >= 0 && *x < self.width as i64 && *y >= 0 && *y < self.height as i64
            })
            .map(|(x, y)| (x as usize, y as usize))
    }

    fn get(&self, (x, y): (usize, usize)) -> Option<u32> {
        if x >= self.width || y >= self.height {
            return None;
        }

        let index = self.width * y + x;
        self.risk_values.get(index).copied()
    }

    fn scale(&self, multiplier: usize) -> RiskGrid {
        let new_width = self.width * multiplier;
        let new_height = self.height * multiplier;
        let mut new_risk_values = Vec::new();

        for y in 0..new_height {
            let tile_distance_y = y / self.height;

            for x in 0..new_width {
                let tile_distance = (x / self.width + tile_distance_y) as u32;
                let base_risk_value = self.get((x % self.width, y % self.height)).unwrap();
                let new_risk_value = (((base_risk_value + tile_distance) - 1) % 9) + 1;
                new_risk_values.push(new_risk_value);
            }
        }

        RiskGrid {
            width: new_width,
            height: new_height,
            risk_values: new_risk_values,
        }
    }
}

impl FromStr for RiskGrid {
    type Err = Box<dyn Error>;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut risk_values = Vec::new();
        let mut width = None;
        let mut height = 0;

        for line in input.lines() {
            let mut row_width = 0;
            height += 1;

            for digit_char in line.chars() {
                row_width += 1;

                let digit = digit_char.to_digit(10).ok_or("Invalid digit!")?;
                risk_values.push(digit);
            }

            if *width.get_or_insert(row_width) != row_width {
                return Err("Row lengths are not all equal to each other.".into());
            }
        }

        let width = width.unwrap_or(0);

        Ok(RiskGrid {
            width,
            height,
            risk_values,
        })
    }
}

impl Display for RiskGrid {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.height {
            for x in 0..self.width {
                write!(f, "{}", self.get((x, y)).unwrap())?;
            }
            writeln!(f)?;
        }

        Ok(())
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct FrontierEntry(u32, (usize, usize), (usize, usize));

impl PartialOrd for FrontierEntry {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.0.cmp(&other.0).reverse())
    }
}

impl Ord for FrontierEntry {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0).reverse()
    }
}

#[cfg(test)]
mod test {
    use crate::RiskGrid;
    use std::error::Error;
    use std::fs::read_to_string;
    use std::str::FromStr;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_15_small")?;
        let grid = RiskGrid::from_str(&input)?;
        let cost = grid.find_path((0, 0), (grid.width - 1, grid.height - 1));

        assert_eq!(Some(40), cost);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_15_small")?;
        let grid = RiskGrid::from_str(&input)?.scale(5);
        let cost = grid.find_path((0, 0), (grid.width - 1, grid.height - 1));

        assert_eq!(Some(315), cost);

        Ok(())
    }
}
