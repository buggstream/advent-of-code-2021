use advent_of_code_2021::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::error::Error;
use std::str::FromStr;

lazy_static! {
    static ref SUBMARINE_COMMAND: Regex = Regex::new(r"(?m)^([[:alpha:]]+) (\d+)$").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(2)?;
    let commands = parse_commands(&input);
    let position_product = puzzle_one(&commands);

    println!("{}", position_product);

    let aim_product = puzzle_two(&commands);

    println!("{}", aim_product);

    Ok(())
}

fn puzzle_one(commands: &[(Direction, i64)]) -> i64 {
    let (mut horizontal, mut vertical) = (0, 0);

    for (direction, amount) in commands.iter().copied() {
        match direction {
            Direction::Forward => horizontal += amount,
            Direction::Up => vertical -= amount,
            Direction::Down => vertical += amount,
        }
    }

    horizontal * vertical
}

fn puzzle_two(commands: &[(Direction, i64)]) -> i64 {
    let (mut horizontal, mut vertical, mut aim) = (0, 0, 0);

    for (direction, amount) in commands.iter().copied() {
        match direction {
            Direction::Forward => {
                horizontal += amount;
                vertical += aim * amount;
            }
            Direction::Up => aim -= amount,
            Direction::Down => aim += amount,
        }
    }

    horizontal * vertical
}

fn parse_commands(input: &str) -> Vec<(Direction, i64)> {
    let mut commands = Vec::new();

    for captures in SUBMARINE_COMMAND.captures_iter(input) {
        let direction = Direction::from_str(captures.get(1).unwrap().as_str()).unwrap();
        let amount = captures.get(2).unwrap().as_str().parse::<i64>().unwrap();

        commands.push((direction, amount));
    }

    commands
}

#[derive(Debug, Copy, Clone)]
enum Direction {
    Forward,
    Up,
    Down,
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "forward" => Ok(Direction::Forward),
            "up" => Ok(Direction::Up),
            "down" => Ok(Direction::Down),
            _ => Err(()),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{parse_commands, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_2_small")?;
        let commands = parse_commands(&input);
        let position_product = puzzle_one(&commands);

        assert_eq!(150, position_product);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_2_small")?;
        let commands = parse_commands(&input);
        let aim_product = puzzle_two(&commands);

        assert_eq!(900, aim_product);

        Ok(())
    }
}
