use advent_of_code_2021::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::cmp::{max, min, Ordering};
use std::collections::{BinaryHeap, HashMap};
use std::error::Error;
use std::str::FromStr;

lazy_static! {
    static ref STARTING_POSITION: Regex =
        Regex::new(r"(?m)^Player \d starting position: (\d)$").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(21)?;
    let starting_positions = parse_starting_positions(&input);
    let score_factor = puzzle_one(starting_positions);
    println!("{}", score_factor);
    let most_universe_wins = puzzle_two(starting_positions);
    println!("{}", most_universe_wins);

    Ok(())
}

fn puzzle_one(start_positions: [u64; 2]) -> u64 {
    let mut positions = start_positions;
    let mut scores = [0; 2];
    let mut turn_count = 1;

    loop {
        let player_index = ((turn_count + 1) % 2) as usize;
        let moves = (17 - (turn_count % 10)) % 10;
        positions[player_index] = (positions[player_index] + moves - 1) % 10 + 1;
        scores[player_index] += positions[player_index];

        if scores[player_index] >= 1000 {
            break scores[(player_index + 1) % 2] * 3 * turn_count;
        }

        turn_count += 1;
    }
}

fn puzzle_two(start_positions: [u64; 2]) -> u64 {
    let start_state = GameState::new(start_positions);
    let mut map = HashMap::new();
    map.insert(start_state, 1);
    let mut heap = BinaryHeap::new();
    heap.push(start_state);
    let mut wins = [0; 2];

    while let Some(state) = heap.pop() {
        let universes = map[&state];

        if let Some(winner) = state.get_winner() {
            wins[winner as usize] += universes;
            continue;
        }

        for (next_state, multiplier) in state.next_states().iter() {
            let new_universes = multiplier * universes;
            let existing_universes = map.entry(*next_state).or_insert_with(|| {
                heap.push(*next_state);
                0
            });

            *existing_universes += new_universes;
        }
    }

    max(wins[0], wins[1])
}

fn parse_starting_positions(input: &str) -> [u64; 2] {
    let mut captures_iter = STARTING_POSITION.captures_iter(input);

    match (captures_iter.next(), captures_iter.next()) {
        (Some(captures_one), Some(captures_two)) => {
            let player_one_start = u64::from_str(&captures_one[1]).unwrap();
            let player_two_start = u64::from_str(&captures_two[1]).unwrap();

            [player_one_start, player_two_start]
        }
        _ => panic!("Couldn't parse starting positions for at least 2 players!"),
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct GameState {
    turn_of: u8,
    positions: [u64; 2],
    scores: [u64; 2],
}

impl GameState {
    fn new(start_positions: [u64; 2]) -> GameState {
        GameState {
            turn_of: 0,
            positions: start_positions,
            scores: [0; 2],
        }
    }

    fn next_states(&self) -> Box<[(GameState, u64); 7]> {
        Box::new([
            (self.move_places(3), 1),
            (self.move_places(4), 3),
            (self.move_places(5), 6),
            (self.move_places(6), 7),
            (self.move_places(7), 6),
            (self.move_places(8), 3),
            (self.move_places(9), 1),
        ])
    }

    fn move_places(&self, places: u64) -> GameState {
        let mut new_state = *self;
        let turn_index = self.turn_of as usize;
        let new_position = (self.positions[turn_index] + places - 1) % 10 + 1;
        new_state.scores[turn_index] += new_position;
        new_state.positions[turn_index] = new_position;
        new_state.turn_of = (self.turn_of + 1) % 2;

        new_state
    }

    fn get_winner(&self) -> Option<u8> {
        if self.scores[0] >= 21 {
            Some(0)
        } else if self.scores[1] >= 21 {
            Some(1)
        } else {
            None
        }
    }

    fn lowest_score(&self) -> u64 {
        min(self.scores[0], self.scores[1])
    }

    fn highest_score(&self) -> u64 {
        max(self.scores[0], self.scores[1])
    }
}

impl PartialOrd for GameState {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(match other.lowest_score().cmp(&self.lowest_score()) {
            Ordering::Equal => other.highest_score().cmp(&self.highest_score()),
            Ordering::Less => Ordering::Less,
            Ordering::Greater => Ordering::Greater,
        })
    }
}

impl Ord for GameState {
    fn cmp(&self, other: &Self) -> Ordering {
        other.lowest_score().cmp(&self.lowest_score())
    }
}

#[cfg(test)]
mod test {
    use crate::{parse_starting_positions, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_21_small")?;
        let starting_positions = parse_starting_positions(&input);
        let score_factor = puzzle_one(starting_positions);

        assert_eq!(739785, score_factor);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_21_small")?;
        let starting_positions = parse_starting_positions(&input);
        let universes = puzzle_two(starting_positions);

        assert_eq!(444356092776315, universes);

        Ok(())
    }
}
