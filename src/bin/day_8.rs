use advent_of_code_2021::read_puzzle_input;
use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(8)?;
    let display_entries = parse_display_entries(&input);
    let unique_segments = puzzle_one(&display_entries);
    println!("{}", unique_segments);
    let output_value_sum = puzzle_two(&display_entries);
    println!("{}", output_value_sum);

    Ok(())
}

fn puzzle_one(display_entries: &[DisplayEntry]) -> usize {
    display_entries
        .iter()
        .map(|display_entry| display_entry.unique_output_digits())
        .sum()
}

fn puzzle_two(display_entries: &[DisplayEntry]) -> u32 {
    display_entries
        .iter()
        .map(|display_entry| display_entry.determine_output_value())
        .sum()
}

fn parse_display_entries(input: &str) -> Vec<DisplayEntry> {
    input
        .lines()
        .map(|line| DisplayEntry::from_str(line).unwrap())
        .collect()
}

#[derive(Debug, Clone)]
struct DisplayEntry {
    signal_patterns: Vec<String>,
    output_values: Vec<String>,
}

impl DisplayEntry {
    fn unique_output_digits(&self) -> usize {
        self.output_values
            .iter()
            .filter(|segments| {
                segments.len() == 2
                    || segments.len() == 3
                    || segments.len() == 4
                    || segments.len() == 7
            })
            .count()
    }

    fn determine_output_value(&self) -> u32 {
        let mut segments = [
            HashSet::<char>::from_iter("abcdefg".chars()),
            HashSet::<char>::from_iter("abcdefg".chars()),
            HashSet::<char>::from_iter("abcdefg".chars()),
            HashSet::<char>::from_iter("abcdefg".chars()),
            HashSet::<char>::from_iter("abcdefg".chars()),
            HashSet::<char>::from_iter("abcdefg".chars()),
            HashSet::<char>::from_iter("abcdefg".chars()),
        ];

        for signal_pattern in self.signal_patterns.iter() {
            match signal_pattern.len() {
                2 => {
                    for index in [2, 5] {
                        segments[index]
                            .retain(|char_signal| signal_pattern.chars().contains(char_signal));
                    }
                }
                3 => {
                    for index in [0, 2, 5] {
                        segments[index]
                            .retain(|char_signal| signal_pattern.chars().contains(char_signal));
                    }
                }
                4 => {
                    for index in [1, 2, 3, 5] {
                        segments[index]
                            .retain(|char_signal| signal_pattern.chars().contains(char_signal));
                    }
                }
                5 => {
                    for index in [0, 3, 6] {
                        segments[index]
                            .retain(|char_signal| signal_pattern.chars().contains(char_signal));
                    }
                }
                6 => {
                    for index in [0, 1, 5, 6] {
                        segments[index]
                            .retain(|char_signal| signal_pattern.chars().contains(char_signal));
                    }
                }
                7 => {
                    for segment in segments.iter_mut() {
                        segment.retain(|char_signal| signal_pattern.chars().contains(char_signal));
                    }
                }
                _ => panic!("Invalid pattern!"),
            }
        }

        let mut segment_mapping = HashMap::new();

        for index in [0, 3, 5, 6, 1, 2, 4] {
            let matched_char = segments[index].drain().next().unwrap();
            segment_mapping.insert(matched_char, ['a', 'b', 'c', 'd', 'e', 'f', 'g'][index]);

            for segment in segments.iter_mut() {
                segment.retain(|char_signal| *char_signal != matched_char);
            }
        }

        self.output_values
            .iter()
            .map(|output_value| {
                let segment_signal = output_value
                    .chars()
                    .map(|obfuscated_char| segment_mapping[&obfuscated_char])
                    .sorted()
                    .join("");

                match segment_signal.as_str() {
                    "abcefg" => 0_u32,
                    "cf" => 1,
                    "acdeg" => 2,
                    "acdfg" => 3,
                    "bcdf" => 4,
                    "abdfg" => 5,
                    "abdefg" => 6,
                    "acf" => 7,
                    "abcdefg" => 8,
                    "abcdfg" => 9,
                    _ => panic!("Invalid segment signal!"),
                }
            })
            .rev()
            .enumerate()
            .fold(0, |acc, (pow, digit)| acc + digit * 10_u32.pow(pow as u32))
    }
}

impl FromStr for DisplayEntry {
    type Err = ();

    fn from_str(line: &str) -> Result<Self, Self::Err> {
        let (patterns, outputs) = line.split_once('|').ok_or(())?;

        let signal_patterns: Vec<_> = patterns
            .trim()
            .split_whitespace()
            .map(|signal_pattern| signal_pattern.chars().sorted().collect::<String>())
            .collect();
        let output_values: Vec<_> = outputs
            .trim()
            .split_whitespace()
            .map(|output| output.chars().sorted().collect::<String>())
            .collect();

        Ok(DisplayEntry {
            signal_patterns,
            output_values,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::{parse_display_entries, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_8_small")?;
        let display_entries = parse_display_entries(&input);
        let unique_segments = puzzle_one(&display_entries);

        assert_eq!(26, unique_segments);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_8_small")?;
        let display_entries = parse_display_entries(&input);
        let output_value_sum = puzzle_two(&display_entries);

        assert_eq!(61229, output_value_sum);

        Ok(())
    }
}
