use advent_of_code_2021::read_puzzle_input;
use std::error::Error;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(6)?;
    let population_array = parse_lantern_fish(&input);
    let fish_population_one = puzzle(population_array, 80);
    let fish_population_two = puzzle(population_array, 256);

    println!("{}", fish_population_one);
    println!("{}", fish_population_two);

    Ok(())
}

fn puzzle(mut fish_population: [u64; 9], days: u64) -> u64 {
    for _ in 0..days {
        let birthing_fish = fish_population[0];
        fish_population.copy_within(1.., 0);
        fish_population[8] = birthing_fish;
        fish_population[6] += birthing_fish;
    }

    fish_population.into_iter().sum()
}

fn parse_lantern_fish(input: &str) -> [u64; 9] {
    let mut population_array = [0; 9];

    for fish_age in input
        .split(',')
        .map(|number_str| usize::from_str(number_str.trim()).unwrap())
    {
        population_array[fish_age] += 1;
    }

    population_array
}

#[cfg(test)]
mod test {
    use crate::{parse_lantern_fish, puzzle};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_6_small")?;
        let population_array = parse_lantern_fish(&input);
        let fish_population = puzzle(population_array.clone(), 80);

        assert_eq!(5934, fish_population);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_6_small")?;
        let population_array = parse_lantern_fish(&input);
        let fish_population = puzzle(population_array.clone(), 256);

        assert_eq!(26984457539, fish_population);

        Ok(())
    }
}
