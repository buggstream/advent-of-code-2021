use advent_of_code_2021::read_puzzle_input;
use std::collections::HashSet;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::ops::RangeInclusive;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(20)?;
    let (algorithm, image) = parse_image(&input);
    let lit_pixels = puzzle(&algorithm, &image, 2);
    println!("{:?}", lit_pixels);
    let lit_pixels = puzzle(&algorithm, &image, 50);
    println!("{:?}", lit_pixels);

    Ok(())
}

fn puzzle(algorithm: &HashSet<usize>, image: &Image, enhance_count: usize) -> Option<usize> {
    let mut image = image.clone();
    for _ in 0..enhance_count {
        image = image.enhance(algorithm);
    }
    image.lit_pixels()
}

fn parse_image(input: &str) -> (HashSet<usize>, Image) {
    let (enhancement_input, image_input) = input.split_once("\n\n").unwrap();
    let enhancement_pixels = enhancement_input
        .chars()
        .enumerate()
        .filter_map(|(index, character)| match character {
            '.' => None,
            '#' => Some(index),
            _ => panic!("Invalid enhancement algorithm character: {:?}", character),
        })
        .collect();
    let image = Image::from_str(image_input).unwrap();

    (enhancement_pixels, image)
}

fn pixel_range((x, y): (i32, i32)) -> [(i32, i32); 9] {
    [
        (x - 1, y - 1),
        (x, y - 1),
        (x + 1, y - 1),
        (x - 1, y),
        (x, y),
        (x + 1, y),
        (x - 1, y + 1),
        (x, y + 1),
        (x + 1, y + 1),
    ]
}

#[derive(Debug, Clone)]
struct Image {
    lit_by_default: bool,
    opposite_pixels: HashSet<(i32, i32)>,
}

impl Image {
    fn enhance(&self, algorithm: &HashSet<usize>) -> Image {
        let lit_by_default = if self.lit_by_default {
            !algorithm.contains(&511) != self.lit_by_default
        } else {
            algorithm.contains(&0) != self.lit_by_default
        };
        let mut opposite_pixels = HashSet::new();
        let (bound_x, bound_y) = self.bounds();

        for y in (bound_y.start() - 1)..=(bound_y.end() + 1) {
            for x in (bound_x.start() - 1)..=(bound_x.end() + 1) {
                let index = self.enhancement_index((x, y));

                if algorithm.contains(&index) != lit_by_default {
                    opposite_pixels.insert((x, y));
                }
            }
        }

        Image {
            lit_by_default,
            opposite_pixels,
        }
    }

    fn enhancement_index(&self, coord: (i32, i32)) -> usize {
        let mut binary_index = 0;

        for pixel_pos in pixel_range(coord) {
            binary_index <<= 1;

            if self.is_lit(&pixel_pos) {
                binary_index |= 1;
            }
        }

        binary_index
    }

    fn is_lit(&self, coord: &(i32, i32)) -> bool {
        self.opposite_pixels.contains(coord) != self.lit_by_default
    }

    fn bounds(&self) -> (RangeInclusive<i32>, RangeInclusive<i32>) {
        let min_x = self
            .opposite_pixels
            .iter()
            .map(|(x, _)| *x)
            .min()
            .unwrap_or(0);
        let max_x = self
            .opposite_pixels
            .iter()
            .map(|(x, _)| *x)
            .max()
            .unwrap_or(0);
        let min_y = self
            .opposite_pixels
            .iter()
            .map(|(_, y)| *y)
            .min()
            .unwrap_or(0);
        let max_y = self
            .opposite_pixels
            .iter()
            .map(|(_, y)| *y)
            .max()
            .unwrap_or(0);

        (min_x..=max_x, min_y..=max_y)
    }

    fn lit_pixels(&self) -> Option<usize> {
        if !self.lit_by_default {
            return Some(self.opposite_pixels.len());
        }

        None
    }
}

impl Display for Image {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let (bound_x, bound_y) = self.bounds();

        for y in bound_y {
            for x in bound_x.clone() {
                if self.is_lit(&(x, y)) {
                    write!(f, "#")?;
                } else {
                    write!(f, ".")?;
                }
            }

            writeln!(f)?;
        }

        Ok(())
    }
}

impl FromStr for Image {
    type Err = Box<dyn Error>;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let light_pixels_result: Result<_, _> = input
            .lines()
            .enumerate()
            .map(|(y, line)| {
                line.chars()
                    .enumerate()
                    .filter_map(move |(x, character)| match character {
                        '.' => None,
                        '#' => Some(Ok((x as i32, y as i32))),
                        _ => Some(Err(format!(
                            "Invalid enhancement algorithm character: {:?}",
                            character
                        ))),
                    })
            })
            .flatten()
            .collect();

        Ok(Image {
            lit_by_default: false,
            opposite_pixels: light_pixels_result?,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::{parse_image, puzzle};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn enhancement_index() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_20_small")?;
        let (_, image) = parse_image(&input);
        let index = image.enhancement_index((2, 2));

        assert_eq!(34, index);

        Ok(())
    }

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_20_small")?;
        let (algorithm, image) = parse_image(&input);
        let lit_pixels = puzzle(&algorithm, &image, 2);

        assert_eq!(Some(35), lit_pixels);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_20_small")?;
        let (algorithm, image) = parse_image(&input);
        let lit_pixels = puzzle(&algorithm, &image, 50);

        assert_eq!(Some(3351), lit_pixels);

        Ok(())
    }
}
