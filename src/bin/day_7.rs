use advent_of_code_2021::read_puzzle_input;
use itertools::Itertools;
use std::cmp::min;
use std::error::Error;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(7)?;
    let crab_submarines = parse_crab_submarines(&input);
    let fuel_used_constant = puzzle_one(crab_submarines.clone());
    println!("{}", fuel_used_constant);
    let fuel_used = puzzle_two(crab_submarines);
    println!("{}", fuel_used);

    Ok(())
}

fn puzzle_one(crab_subs: Vec<(i64, i64)>) -> i64 {
    if crab_subs.is_empty() {
        return 0;
    }

    let mut minimum_fuel = i64::MAX;
    for (position, _) in crab_subs.iter() {
        let fuel_used: i64 = crab_subs
            .iter()
            .copied()
            .map(|(crab_pos, amount)| (*position - crab_pos).abs() * amount)
            .sum();

        minimum_fuel = min(minimum_fuel, fuel_used);
    }

    minimum_fuel
}

fn puzzle_two(crab_subs: Vec<(i64, i64)>) -> i64 {
    if crab_subs.is_empty() {
        return 0;
    }

    let (min_pos, _) = crab_subs[0];
    let (max_pos, _) = crab_subs[crab_subs.len() - 1];

    let mut minimum_fuel = i64::MAX;
    for position in min_pos..=max_pos {
        let fuel_used: i64 = crab_subs
            .iter()
            .copied()
            .map(|(crab_pos, amount)| {
                let distance = (position - crab_pos).abs();
                let base_cost = distance * (distance + 1) / 2;
                base_cost * amount
            })
            .sum();

        minimum_fuel = min(minimum_fuel, fuel_used);
    }

    minimum_fuel
}

fn parse_crab_submarines(input: &str) -> Vec<(i64, i64)> {
    let mut numbers: Vec<_> = input
        .split(',')
        .map(|number_str| i64::from_str(number_str.trim()).unwrap())
        .collect();

    numbers.sort_unstable();

    let mut crab_subs = Vec::new();

    for (position, group) in &numbers.into_iter().group_by(|crab_pos| *crab_pos) {
        crab_subs.push((position, group.count() as i64));
    }

    crab_subs
}

#[cfg(test)]
mod test {
    use crate::{parse_crab_submarines, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_7_small")?;
        let crab_submarines = parse_crab_submarines(&input);
        let fuel_used = puzzle_one(crab_submarines);

        assert_eq!(37, fuel_used);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_7_small")?;
        let crab_submarines = parse_crab_submarines(&input);
        let fuel_used = puzzle_two(crab_submarines);

        assert_eq!(168, fuel_used);

        Ok(())
    }
}
