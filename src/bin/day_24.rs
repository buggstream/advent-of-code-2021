use advent_of_code_2021::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::VecDeque;
use std::error::Error;
use std::fmt::{Debug, Formatter};
use std::fs::read_to_string;
use std::str::FromStr;

lazy_static! {
    static ref INSTRUCTIONS: Regex =
        Regex::new(r"(?m)^(inp|add|mul|div|mod|eql) ([wxyz])(?: ([wxyz]|-?\d+))?$").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(24)?;
    // let input = read_to_string("additional_inputs/day_24_bits")?;
    let program = parse_instructions(&input)?;
    let model_number = puzzle_one(&program);

    println!("{:?}", model_number);

    Ok(())
}

fn puzzle_one(program: &[Instruction]) -> Option<i64> {
    for number in (99_999_900_000_000..=99_999_999_999_999).rev() {
        let mut alu = Alu::new();
        let mut inputs = VecDeque::from(to_digits(number));

        alu.execute_all(program, &mut || {
            inputs.pop_front().map(|digit| digit as i64)
        })
            .ok()?;

        if alu.var_to_int(Var::Z) == 0 {
            return Some(number);
        }
    }

    None
}

fn parse_instructions(input: &str) -> Result<Vec<Instruction>, Box<dyn Error>> {
    let mut instructions = Vec::new();

    for captures in INSTRUCTIONS.captures_iter(input) {
        let first_arg = match &captures[2] {
            "w" => Var::W,
            "x" => Var::X,
            "y" => Var::Y,
            "z" => Var::Z,
            _ => unreachable!("Regex only captures w, x, y or z characters."),
        };

        let second_arg_opt = match captures.get(3).map(|m| m.as_str()) {
            None => None,
            Some("w") => Some(Arg::Var(Var::W)),
            Some("x") => Some(Arg::Var(Var::X)),
            Some("y") => Some(Arg::Var(Var::Y)),
            Some("z") => Some(Arg::Var(Var::Z)),
            Some(number_str) => Some(Arg::Value(i64::from_str(number_str)?)),
        };

        let instruction = match (&captures[1], second_arg_opt) {
            ("inp", None) => Instruction::Inp(first_arg),
            ("add", Some(second_arg)) => Instruction::Add(first_arg, second_arg),
            ("mul", Some(second_arg)) => Instruction::Mul(first_arg, second_arg),
            ("div", Some(second_arg)) => Instruction::Div(first_arg, second_arg),
            ("mod", Some(second_arg)) => Instruction::Mod(first_arg, second_arg),
            ("eql", Some(second_arg)) => Instruction::Eql(first_arg, second_arg),
            _ => unreachable!("Regex should only captures valid instructions."),
        };

        instructions.push(instruction);
    }

    Ok(instructions)
}

fn to_digits(mut number: i64) -> Vec<i8> {
    let mut digits = Vec::new();

    loop {
        digits.push((number % 10) as i8);
        number /= 10;

        if number == 0 {
            return digits;
        }
    }
}

#[derive(Clone, Eq, PartialEq)]
struct Alu {
    variables: [i64; 4],
}

impl Alu {
    fn new() -> Alu {
        Alu { variables: [0; 4] }
    }

    fn execute_all<F: FnMut() -> Option<i64>>(
        &mut self,
        instructions: &[Instruction],
        read_input: &mut F,
    ) -> Result<(), AluError> {
        for inst in instructions {
            self.execute(*inst, read_input)?;
        }

        Ok(())
    }

    fn execute<F: FnMut() -> Option<i64>>(
        &mut self,
        inst: Instruction,
        read_input: &mut F,
    ) -> Result<(), AluError> {
        match inst {
            Instruction::Inp(var) => {
                let input = read_input().ok_or(AluError::OutOfInputs)?;
                *self.var_ref_mut(var) = input;
            }
            Instruction::Add(var, arg) => {
                *self.var_ref_mut(var) += self.arg_to_int(arg);
            }
            Instruction::Mul(var, arg) => {
                *self.var_ref_mut(var) *= self.arg_to_int(arg);
            }
            Instruction::Div(var, arg) => {
                let int_arg = self.arg_to_int(arg);
                let base = self.var_ref_mut(var);

                if int_arg == 0 {
                    return Err(AluError::DivideByZero);
                }

                *base /= int_arg;
            }
            Instruction::Mod(var, arg) => {
                let int_arg = self.arg_to_int(arg);
                let base = self.var_ref_mut(var);

                if *base < 0 {
                    return Err(AluError::ModuloNegativeBase);
                }

                if int_arg <= 0 {
                    return Err(AluError::ModuloNegativeArg);
                }

                *base %= int_arg;
            }
            Instruction::Eql(var, arg) => {
                let int_arg = self.arg_to_int(arg);
                let var_ref = self.var_ref_mut(var);

                *var_ref = (*var_ref == int_arg) as i64;
            }
        }

        Ok(())
    }

    #[inline]
    fn arg_to_int(&self, arg: Arg) -> i64 {
        match arg {
            Arg::Value(int) => int,
            Arg::Var(var) => self.var_to_int(var),
        }
    }

    #[inline]
    fn var_to_int(&self, var: Var) -> i64 {
        match var {
            Var::W => self.variables[0],
            Var::X => self.variables[1],
            Var::Y => self.variables[2],
            Var::Z => self.variables[3],
        }
    }

    #[inline]
    fn var_ref_mut(&mut self, var: Var) -> &mut i64 {
        match var {
            Var::W => &mut self.variables[0],
            Var::X => &mut self.variables[1],
            Var::Y => &mut self.variables[2],
            Var::Z => &mut self.variables[3],
        }
    }
}

impl From<[i64; 4]> for Alu {
    fn from(variables: [i64; 4]) -> Self {
        Alu { variables }
    }
}

impl Debug for Alu {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Alu")
            .field("w", &self.variables[0])
            .field("x", &self.variables[1])
            .field("y", &self.variables[2])
            .field("z", &self.variables[3])
            .finish()
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum AluError {
    OutOfInputs,
    DivideByZero,
    ModuloNegativeBase,
    ModuloNegativeArg,
}

#[derive(Debug, Copy, Clone)]
enum Instruction {
    Inp(Var),
    Add(Var, Arg),
    Mul(Var, Arg),
    Div(Var, Arg),
    Mod(Var, Arg),
    Eql(Var, Arg),
}

#[derive(Debug, Copy, Clone)]
enum Arg {
    Value(i64),
    Var(Var),
}

#[derive(Debug, Copy, Clone)]
enum Var {
    W,
    X,
    Y,
    Z,
}

#[cfg(test)]
mod test {
    use crate::{parse_instructions, Alu, AluError};
    use std::collections::VecDeque;
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn bits_ok() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_24_bits")?;
        let instructions = parse_instructions(&input)?;
        let mut alu = Alu::new();
        let mut inputs = VecDeque::from([0b1101]);

        let result = alu.execute_all(&instructions, &mut || inputs.pop_front());

        assert_eq!(Ok(()), result);
        assert_eq!(Alu::from([1, 1, 0, 1]), alu);

        Ok(())
    }

    #[test]
    fn bits_mod_error() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_24_bits")?;
        let instructions = parse_instructions(&input)?;
        let mut alu = Alu::new();
        let mut inputs = VecDeque::from([-1]);

        let result = alu.execute_all(&instructions, &mut || inputs.pop_front());

        assert_eq!(Err(AluError::ModuloNegativeBase), result);

        Ok(())
    }
}
