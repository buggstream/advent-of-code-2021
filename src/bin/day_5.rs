use advent_of_code_2021::read_puzzle_input;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::error::Error;
use std::str::FromStr;

lazy_static! {
    static ref LINE_SEGMENT: Regex = Regex::new(r"(?m)^(\d+),(\d+) -> (\d+),(\d+)$").unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(5)?;
    let segments = parse_line_segments(&input);
    let simple_points_overlap = puzzle_one(&segments);
    println!("{}", simple_points_overlap);
    let points_overlap = puzzle_two(&segments);
    println!("{}", points_overlap);

    Ok(())
}

fn puzzle_one(segments: &[(Point, Point)]) -> usize {
    let mut map = HashMap::new();

    for ((start_x, start_y), (end_x, end_y)) in segments
        .iter()
        .copied()
        .filter(|((start_x, start_y), (end_x, end_y))| *start_x == *end_x || *start_y == *end_y)
    {
        if start_x == end_x {
            let iter_y = if start_y < end_y {
                start_y..=end_y
            } else {
                end_y..=start_y
            };

            for current_y in iter_y {
                let count = map.entry((start_x, current_y)).or_insert(0_u32);
                *count += 1;
            }
        } else {
            let iter_x = if start_x < end_x {
                start_x..=end_x
            } else {
                end_x..=start_x
            };

            for current_x in iter_x {
                let count = map.entry((current_x, start_y)).or_insert(0_u32);
                *count += 1;
            }
        }
    }

    map.values().filter(|count| **count >= 2).count()
}

fn puzzle_two(segments: &[(Point, Point)]) -> usize {
    let mut map = HashMap::new();

    for ((start_x, start_y), (end_x, end_y)) in segments.iter().copied() {
        if start_x == end_x {
            let iter_y = if start_y < end_y {
                start_y..=end_y
            } else {
                end_y..=start_y
            };

            for current_y in iter_y {
                let count = map.entry((start_x, current_y)).or_insert(0_u32);
                *count += 1;
            }
        } else if start_y == end_y {
            let iter_x = if start_x < end_x {
                start_x..=end_x
            } else {
                end_x..=start_x
            };

            for current_x in iter_x {
                let count = map.entry((current_x, start_y)).or_insert(0_u32);
                *count += 1;
            }
        } else {
            let iter_x: Box<dyn Iterator<Item = u32>> = if start_x < end_x {
                Box::new(start_x..=end_x)
            } else {
                Box::new((end_x..=start_x).rev())
            };

            let iter_y: Box<dyn Iterator<Item = u32>> = if start_y < end_y {
                Box::new(start_y..=end_y)
            } else {
                Box::new((end_y..=start_y).rev())
            };

            for (current_x, current_y) in iter_x.zip(iter_y) {
                let count = map.entry((current_x, current_y)).or_insert(0_u32);
                *count += 1;
            }
        }
    }

    map.values().filter(|count| **count >= 2).count()
}

fn parse_line_segments(input: &str) -> Vec<(Point, Point)> {
    let mut segments = Vec::new();

    for captures in LINE_SEGMENT.captures_iter(input) {
        let start_x = u32::from_str(&captures[1]).unwrap();
        let start_y = u32::from_str(&captures[2]).unwrap();
        let end_x = u32::from_str(&captures[3]).unwrap();
        let end_y = u32::from_str(&captures[4]).unwrap();

        segments.push(((start_x, start_y), (end_x, end_y)));
    }

    segments
}

type Point = (u32, u32);

#[cfg(test)]
mod test {
    use crate::{parse_line_segments, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_5_small")?;
        let segments = parse_line_segments(&input);
        let points_overlap = puzzle_one(&segments);

        assert_eq!(5, points_overlap);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_5_small")?;
        let segments = parse_line_segments(&input);
        let points_overlap = puzzle_two(&segments);

        assert_eq!(12, points_overlap);

        Ok(())
    }
}
