use std::error::Error;
use std::fs::File;
use std::io::{Read, Write};
use std::time::Duration;

const YEAR: u32 = 2021;
const DAY: u32 = 24;

fn main() -> Result<(), Box<dyn Error>> {
    let url = get_input_url(YEAR, DAY);
    let mut credentials = String::new();
    File::open("credentials.secret")?.read_to_string(&mut credentials)?;
    let input = ureq::get(&url)
        .set("Cookie", &format!("session={}", &credentials))
        .timeout(Duration::from_secs(30))
        .call()?
        .into_string()?;

    let mut puzzle_input = File::create(format!("inputs/day_{}", DAY))?;
    puzzle_input.write_all(input.as_bytes())?;

    Ok(())
}

fn get_input_url(year: u32, day: u32) -> String {
    format!("https://adventofcode.com/{}/day/{}/input", year, day)
}
