use advent_of_code_2021::read_puzzle_input;
use std::cmp::max;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(3)?;
    let (numbers, max_bits) = parse_binary_numbers(&input);
    let power_consumption = puzzle_one(&numbers, max_bits);

    println!("{}", power_consumption);

    let life_support_rating = puzzle_two(&numbers, max_bits);

    println!("{}", life_support_rating);

    Ok(())
}

fn puzzle_one(numbers: &[u32], max_bits: usize) -> u32 {
    let mut gamma_rate = 0;
    let mut epsilon_rate = 0;

    for offset in 0..max_bits {
        let mask = 1_u32 << offset;
        let one_occurrences = numbers
            .iter()
            .copied()
            .map(|number| number & mask)
            .filter(|masked_number| *masked_number > 0)
            .count();

        if one_occurrences > (numbers.len() - one_occurrences) {
            gamma_rate += mask;
        } else {
            epsilon_rate += mask;
        }
    }

    gamma_rate * epsilon_rate
}

fn puzzle_two(numbers: &[u32], max_bits: usize) -> u32 {
    let oxygen_generator_rating = oxygen_generator_rating(numbers, max_bits).unwrap();
    let co2_scrubber_rating = co2_scrubber_rating(numbers, max_bits).unwrap();

    oxygen_generator_rating * co2_scrubber_rating
}

fn oxygen_generator_rating(numbers: &[u32], max_bits: usize) -> Option<u32> {
    if numbers.is_empty() {
        return None;
    }

    let mut filtered_numbers: Vec<_> = numbers.iter().copied().collect();

    for offset in 0..max_bits {
        if filtered_numbers.len() == 1 {
            return Some(filtered_numbers[0]);
        }

        let mask = (1_u32 << (max_bits - 1)) >> offset;
        let one_occurrences = filtered_numbers
            .iter()
            .copied()
            .map(|number| number & mask)
            .filter(|masked_number| *masked_number > 0)
            .count();

        if one_occurrences >= (filtered_numbers.len() - one_occurrences) {
            filtered_numbers.retain(|number| *number & mask > 0);
        } else {
            filtered_numbers.retain(|number| *number & mask == 0);
        }
    }

    if filtered_numbers.len() == 1 {
        return Some(filtered_numbers[0]);
    }

    None
}

fn co2_scrubber_rating(numbers: &[u32], max_bits: usize) -> Option<u32> {
    if numbers.is_empty() {
        return None;
    }

    let mut filtered_numbers: Vec<_> = numbers.iter().copied().collect();

    for offset in 0..max_bits {
        if filtered_numbers.len() == 1 {
            return Some(filtered_numbers[0]);
        }

        let mask = (1_u32 << (max_bits - 1)) >> offset;
        let zero_occurrences = filtered_numbers
            .iter()
            .copied()
            .map(|number| number & mask)
            .filter(|masked_number| *masked_number == 0)
            .count();

        if zero_occurrences <= (filtered_numbers.len() - zero_occurrences) {
            filtered_numbers.retain(|number| *number & mask == 0);
        } else {
            filtered_numbers.retain(|number| *number & mask > 0);
        }
    }

    if filtered_numbers.len() == 1 {
        return Some(filtered_numbers[0]);
    }

    None
}

fn parse_binary_numbers(input: &str) -> (Vec<u32>, usize) {
    let mut numbers = Vec::new();
    let mut max_bits = 0;

    for line in input.lines() {
        max_bits = max(max_bits, line.len());

        let number = u32::from_str_radix(line, 2).unwrap();
        numbers.push(number);
    }

    (numbers, max_bits)
}

#[cfg(test)]
mod test {
    use crate::{parse_binary_numbers, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_3_small")?;
        let (numbers, max_bits) = parse_binary_numbers(&input);
        let power_consumption = puzzle_one(&numbers, max_bits);

        assert_eq!(198, power_consumption);

        Ok(())
    }

    #[test]
    fn puzzle_two_small() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("additional_inputs/day_3_small")?;
        let (numbers, max_bits) = parse_binary_numbers(&input);
        let life_support_rating = puzzle_two(&numbers, max_bits);

        assert_eq!(230, life_support_rating);

        Ok(())
    }
}
