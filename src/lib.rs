use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::ops::Sub;
use std::str::FromStr;

pub fn read_puzzle_input(day: u32) -> Result<String, Box<dyn Error>> {
    let file_name = format!("inputs/day_{}", day);
    let mut file_contents = String::new();
    File::open(&file_name)?.read_to_string(&mut file_contents)?;
    Ok(file_contents)
}

pub trait ParseList: FromStr {
    fn parse_list(input: &str) -> Result<Vec<Self>, Self::Err> {
        input.lines().map(|line| FromStr::from_str(line)).collect()
    }
}

impl ParseList for u64 {}
impl ParseList for u32 {}

impl ParseList for i64 {}
impl ParseList for i32 {}

pub trait AbsoluteDifference: Sized + Sub<Output = Self> + PartialOrd {
    fn diff_abs(self, other: Self) -> Self {
        if self > other {
            self - other
        } else {
            other - self
        }
    }
}

impl AbsoluteDifference for u8 {}
impl AbsoluteDifference for u16 {}
impl AbsoluteDifference for u32 {}
impl AbsoluteDifference for u64 {}
impl AbsoluteDifference for u128 {}
impl AbsoluteDifference for usize {}

pub fn try_collect_array<T: Default + Copy, const N: usize>(iter: impl IntoIterator<Item = T>) -> Option<[T; N]> {
    let mut iterator = iter.into_iter();
    let mut array = [T::default(); N];

    for element in array.iter_mut() {
        *element = iterator.next()?;
    }

    Some(array)
}
